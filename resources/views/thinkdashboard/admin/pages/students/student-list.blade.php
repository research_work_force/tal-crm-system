@extends('thinkdashboard.admin.layouts.dashboard-layout')
@section('content')


        <!-- Begin Page Content -->
        <div class="container-fluid">


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">List of Students</h6>
            </div>
            <div class="card-body">
              <form action="{{route('students.list.filterName')}}" method="POST">
                {{ csrf_field() }}
              <label><b>Search by name:</b></label>
              <input style="border: 1px solid #ccc; border-radius: 4px;" type="text" name="student-name" placeholder="Student's name" required="">
              <button class="btn btn-sm btn-info" type="submit"><b>Search</b></button>
             </form>
             <form action="{{route('students.list.filterSort')}}" method="POST">
              {{ csrf_field() }}
              <label><b>Sort by</b></label>
                <select name="sortByName" id="mySelect" onchange="this.form.submit()">
                <option value="" selected disabled>....</option>
                <option value="a">Alpha(a-z)</option>
                <option value="z">Alpha(z-a)</option>
                </select>
              </form>
              <!-- <form action="{{route('students.list.filterDate')}}" method="POST">
                {{ csrf_field() }}
                <p><label><b>Search by date:</b></label>
                  <input type="date" name="d1">to <input type="date" name="d2">
                  <button class="btn btn-sm btn-info" type="submit"><b>Search</b></button>
              </form> -->

              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  <tr>
                    <th>Image</th>
                    <th>Student Code</th>
                    <th>Email</th>
                    <th>Student Name</th>
                    <th>Added by</th>
                    <th>Contact</th>
                    <th>Address</th>
                    <th>Gardian Name</th>
                    <th>Gardian Number</th>
                    <th>Gardian Email</th>
                    <th>Institue</th>
                    <th>Future Goal</th>
                    <th>Adhaar Doc(Link)</th>
                    <th>Other Docs</th>
                    <th>Status</th>
                  </tr>
                  </thead>

                  <tbody>
                  @foreach($student_data as $user)
                  <tr>
                     <td>
                      @if (is_null($user->photo))
                      <img src="{{asset('upload/student/photo/default.png')}}" height="50" width="50">
                      @else
                      <img src="{{asset('upload/student/photo')}}/{{$user->photo}}" height="50" width="50">                      

                      @endif
                     </td>                    

                      <td>{{$user->student_code}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->student_name}}</td>
                      <td>{{$user->added_by}}</td>
                      <td>{{$user->contact}}</td>
                      <td>{{$user->address}}</td>
                      <td>{{$user->g_name}}</td>
                      <td>{{$user->g_contact}}</td>
                      <td>{{$user->g_email}}</td>
                      <td>{{$user->institute_name}}</td>
                      <td>{{$user->future_goal}}</td>
                      <td>
                      
                        <a class="btn btn-sm btn-info" href="{{ asset('upload/student/adhaar_card') }}/{{$user->adhaar_card}}" target="_blank">View</a></td>

                        <td>
                       
                        @if ($user->other_docs != 'default.png')
                        <a class="btn btn-sm btn-info" href="{{ asset('upload/student/other_docs') }}/{{$user->other_docs}}" target="_blank">View</a></td>
                        @else
                           No Documents
                        @endif


                        <td>
                             
                        </td>                      
                    </tr>
                    @endforeach
                 </tbody>
                </table>
              </div>
            </div>
           <nav aria-label="Page navigation example">
                {{ $student_data->render("pagination::bootstrap-4") }}
               </nav>
          </div>
        </div>
@endsection

