
@extends('thinkdashboard.admin.layouts.dashboard-layout')
@section('content')

<div class="container">

  <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
         <!-- Nested Row within Card Body -->
         <div class="row">

            <div class=" col-lg-12">
              <div class="container">
                <div class="p-5">
                      <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">Create a coupon</h1>
                      </div>

                  <form method="POST" action="{{route('create.coupon')}}">
                    {{csrf_field()}}

                   <!-- 1st row -->

                     <div class="row">
                       <div class="col-md-3">
                        <div>
                        <input class="form-control my-2 form-control-user" id="code" name="coupon_code" type="text" value="{{ old('coupon_code') }}" placeholder="coupon code here" required>
                        <p style="color:red;">{{ $errors->first('coupon_code') }}</p>
                        </div>

                      </div>
                      <!-- /.col -->
                      <div class="col-md-9">
                        <div>
                        <textarea class="form-control my-2 form-control-user" id="coupon-name" name="coupon_description" type="name" placeholder=" coupon description here" required>{{ old('coupon_description') }}</textarea>
                        <p style="color:red;">{{ $errors->first('coupon_description') }}</p>
                        </div>

                      </div>
                      <!-- /.col -->

                     </div>
                    <!-- /.row -->
                    <!-- 2nd row -->
                    <div class="row">
                        <div class="col-md-3">
                                    <div>
                                    <input class="form-control form-control-user" id="d-percentage" name="discount_percentage" type="number" value="{{ old('discount_percentage') }}" placeholder="percentage" required>
                                    <p style="color:red;">{{ $errors->first('discount_percentage') }}</p>
                                    </div>

                        </div>
                        <!-- /.col -->

                        <div class="col-md-5">
                                    <div class="form-group">
                                                <select class="form-control"  name="course_code">
                                                  <option selected disabled>Course Name:</option>
                                                @foreach ($course as $courses)
                                                <option value="{{$courses->course_code}}">{{$courses->course_name}}</option>
                                                @endforeach
                                                </select>
                                                <p style="color:red;">{{ $errors->first('course_code') }}</p>

                                    </div>
                       </div>

                        <!-- /.col -->
                        <div class="col-md-4">
                                    <div class="form-group">
                                                <select class="form-control"  name="franchise_user_name">
                                                  <option selected disabled>Franchise Name</option>
                                                  @foreach ($fran as $franc)
                                                  <option value="{{$franc->user_name}}">{{$franc->name}}</option>
                                                  @endforeach
                                                </select>
                                                <p style="color:red;">{{ $errors->first('franchise_user_name') }}</p>

                                    </div>
                        </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->


                       <div class="mt-3">
                       <button type="submit" class="btn btn-primary ">Submit</button>
                       </div>

                  </form>
                 <!-- register form end -->
                </div>
                {{-- /.p-5 --}}
              </div>
              {{-- /.container --}}
            </div>
            {{-- /.col --}}
         </div>
         {{-- /.row --}}
      </div>
      {{-- /.card-body --}}
 </div>
 {{-- /.card --}}

</div>
{{-- /.conatiner --}}





@endsection
