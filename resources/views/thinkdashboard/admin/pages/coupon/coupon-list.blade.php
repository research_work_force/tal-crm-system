@extends('thinkdashboard.admin.layouts.dashboard-layout')
@section('content')

 <!-- Begin Page Content -->
        <div class="container-fluid">


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Coupons</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  <tr>
                    <th>Coupon Code</th>
                    <th>Coupon Details</th>
                    <th>Discount Percentage</th>
                    <th>Discount Price</th>
                    <th>Course Name</th>
                    <th>Franchise Name</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>

                  <tbody>
                  @foreach($users as $user)
                  <tr>
                      <td>{{$user->coupon_code}}</td>
                      <td>{{$user->coupon_description}}</td>
                      <td>{{$user->discount_percentage}}%</td>
                      <td><i class="fas fa-rupee-sign"></i> {{$user->discount_price}}</td>
                      <td>{{$user->course_name}}</td>
                      <td>{{$user->name}}</td>
                      <td>
                        {{$user->coupon_status ? 'Activated' : 'Deactivated'}}
                        </td>
                      <td>
                            <!-- Edit delete -->
                         <div class="d-flex flex-row">
                            <a class = "btn btn-sm btn-info text-white mr-2" type="submit" data-toggle="modal" data-target="#modal-edit{{$user->coupon_code}}">Edit</a>
                            @include('thinkdashboard.admin.include.modal.coupon-edit-modal',['users'=>$user])
                                @if($user->coupon_status == 1)
                                <a href="/admin/coupon/block/{{$user->coupon_code}}" class = "btn btn-sm btn-danger text-white mr-2" type="submit">Deactivate</a>
                              @else
                                <a href="/admin/coupon/block/{{$user->coupon_code}}" class = "btn btn-sm btn-success text-white mr-2" type="submit">Activate</a>
                              @endif

                               </div>
                                <div class="delete">
                                    <a type="submit" class = "btn btn-sm btn-danger text-white"  data-toggle="modal" data-target="#modal-delete{{$user->coupon_code}}">delete</a>
                                    @include('thinkdashboard.admin.include.modal.coupon-delete-modal',['coupon_code'=>$user->coupon_code])
                                </div>
                         </div>
                    </td>
                    </tr>
                    @endforeach
                 </tbody>
                </table>
              </div>
            </div>
           <nav aria-label="Page navigation example">
                  {{$users->render("pagination::bootstrap-4") }}
               </nav>
          </div>
        </div>
@endsection
