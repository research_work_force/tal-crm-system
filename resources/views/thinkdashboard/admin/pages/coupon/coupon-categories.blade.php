@extends('thinkdashboard.admin.layouts.dashboard-layout')
@section('content')

<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-md-12">
                <div class="container">
                <div class="p-5">
                    <h4 class="text-center mb-3">Coupons (Category by Franchise)</h6>
                    <form action="/admin/coupon/list/categories" method="POST">
                        @csrf
                        <div class="row">
                            {{-- franchise --}}
                            <div class="col-md-12">
                                    <div class="form-group">
                                            <select class="form-control"  name="franchise_user_name">
                                                    <option selected disabled>Franchise Name</option>
                                                    @foreach ($fran as $franc)
                                                    <option value="{{$franc->user_name}}">{{$franc->name}}</option>
                                                    @endforeach
                                            </select>
                                            <p style="color:red;">{{ $errors->first('franchise_name') }}</p>
                                    </div>

                            </div>
                        </div>

                        <button  class="btn btn-info" type="submit">Submit</button>


                    </form>






                </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
