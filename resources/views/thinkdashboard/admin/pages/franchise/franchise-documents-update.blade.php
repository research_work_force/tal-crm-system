@extends('thinkdashboard.admin.layouts.dashboard-layout')
@section('content')





        <!-- Begin Page Content -->
        <div class="container-fluid">


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">User Table</h6>
            </div>
            <div class="card-body">

                <div class="clearfix my-2">
                    <div class="float-right">
                        <a class="btn btn-info btn-circle"  data-toggle="modal"  data-target = "#franchise-doc-edit"><i class="fas fa-plus"></i></a>
                    </div>
                </div>
                @include('thinkdashboard.franchise.include.modal.franchise-document-edit-modal',['user_name'=>$user_name])

              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  <tr>
                    <th>Name of Documents</th>
                    <th>Documents</th>
                    <th>Delete</th>
                  </tr>
                  </thead>

                  <tbody>
                    @foreach($documents as $docs)
                    <tr>

                        @if ($user_name == $docs->franchise_user_name)
                        <td>{{$docs->documents}}</td>
                        <td>
                            @php
                                $data =$docs->documents;
                                list($name,$ext)=explode(".",$data);
                            @endphp
                            @if ( $ext == 'pdf')
                        <iframe class="img-in-table-fran-update"  src=" {{asset('upload/documents/'.$user_name.'/'.$docs->documents)}} "></iframe>
                            @else
                        <img class="img-responsive img-fluid img-in-table-fran-update" src="{{asset('upload/documents/'.$user_name.'/'.$docs->documents)}}" alt="Image" >
                            @endif
                        </td>
                    <td><a class="btn btn-sm btn-danger" href="/admin/franchise/documents/delete/{{$docs->id}}">Delete</a>
                    </td>
                        @endif

                    </tr>
                    @endforeach
                 </tbody>
                </table>
              </div>
              {{-- /.table --}}

              <div class="go-to-list">
                  <a class="btn btn-info btn-sm" href="/admin/franchise/list"> Go to list <i class="fas fa-arrow-right"></i> </a>
              </div>

            </div>
           <nav aria-label="Page navigation example">
                  <ul class="pagination justify-content-end pull-right">
                    <li class="page-item disabled">
                      <a class="page-link" href="#" tabindex="-1">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#">Next</a>
                    </li>
                  </ul>
               </nav>
          </div>
        </div>
@endsection
