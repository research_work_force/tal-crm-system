@extends('thinkdashboard.admin.layouts.dashboard-layout')
@section('content')






        <!-- Begin Page Content -->
        <div class="container-fluid">


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Franchise List</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>User Name</th>
                    <th>Phone Number</th>
                    <th>Location</th>
                    <th>Agreement Date</th>
                    <th>PF/ESI</th>
                    <th>Official Number</th>
                    <th>Franchise Account Managers</th>
                    <th>Trade Licence</th>
                    <th>IFSC Code</th>
                    <th>Bank Name</th>
                    <th>Branch Name</th>
                    <th>PAN</th>
                    <th>TAN</th>
                    <th>GST</th>
                    <th>Documents</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>

                  <tbody>
                  @foreach($users as $user)
                  <tr>
                      <td>{{$user->name}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->user_name}}</td>
                      <td>{{$user->phone}}</td>
                      <td>{{$user->location}}</td>
                      <td>{{$user->agreement_date}}</td>
                      <td>{{$user->pf_esi}}</td>
                      <td>{{$user->oficial_number}}</td>
                      <td>{{$user->franchise_account_manager}}</td>
                      <td>{{$user->trade_license}}</td>
                      <td>{{$user->ifsc_code}}</td>
                      <td>{{$user->bank_name}}</td>
                      <td>{{$user->branch_name}}</td>
                      <td>{{$user->pan}}</td>
                      <td>{{$user->tan}}</td>
                      <td>{{$user->gst}}</td>
                      <td>
                        @foreach ($documents as $item)
                        @if ($user->user_name==$item->franchise_user_name)

                            <a href="{{ asset('upload/documents/'.$user->user_name.'/'.$item->documents) }}">{{$item->documents}}</a>

                        @endif
                        @endforeach

                        ||
                         <a href="/admin/franchise/documents/list/{{$user->user_name}}" class = "btn btn-sm btn-info text-white mr-2"><i class="fa fa-upload" aria-hidden="true"></i></a>
                    </td>
                      <td>
                          @if ($user->status == 1)
                            Unblocked
                          @elseif($user->status == 0)
                            Blocked
                          @else
                            Deleted
                          @endif
                        </td>
                      <td>
                        
                         <a class = "btn btn-sm btn-primary text-white mr-2"  data-toggle="modal" data-target="#modal-edit{{$user->franchise_id}}"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                
                                  
                                     @include('thinkdashboard.admin.include.modal.franchise-edit-modal',['users'=>$user])
                         <br>
                         <br>

                        @if($user->status == 1)
                                  
                                     <a href="/admin/franchise/block/{{$user->user_name}}" class = "btn btn-sm btn-danger text-white mr-2" ><i class="fa fa-ban" aria-hidden="true"></i></a>
                                     <br>
                                     <br>
                                     <a  class = "btn btn-sm btn-warning text-white mr-2"  data-toggle="modal" data-target="#modal-f-delete{{$user->franchise_id}}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                     @include('thinkdashboard.admin.include.modal.franchise-delete-modal',['user_name'=>$user->franchise_id])
                           
                                 
                                     
                                @elseif($user->status == 0)
                                
                                     <a href="/admin/franchise/block/{{$user->user_name}}" class = "btn btn-sm btn-success text-white mr-2"><i class="fa fa-unlock" aria-hidden="true"></i></a>
                                   
                                     <br>
                                     <br>

                                     <a  class = "btn btn-sm btn-danger text-white mr-2" data-toggle="modal" data-target="#modal-f-delete{{$user->franchise_id}}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                     @include('thinkdashboard.admin.include.modal.franchise-delete-modal',['user_name'=>$user->franchise_id])
                                
                                   
                                @else
                                    
                                @endif
                                
                     
                    </td>
                    </tr>
                    @endforeach
                 </tbody>
                </table>
                
                               
              </div>
            </div>
           <nav aria-label="Page navigation example">
                    {{ $users->render("pagination::bootstrap-4") }}
               </nav>
          </div>
        </div>
@endsection
