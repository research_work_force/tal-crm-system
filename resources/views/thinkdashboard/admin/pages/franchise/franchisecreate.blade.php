@extends('thinkdashboard.admin.layouts.dashboard-layout')
@section('content')


<div class="container">

<div class="card o-hidden border-0 shadow-lg my-5">
  <div class="card-body p-0">
    <!-- Nested Row within Card Body -->
    <div class="row">
      <!-- <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
      <div class=" col-lg-12">
        <div class="container">
        <div class="p-5">
          <div class="text-center">
            <h1 class="h4 text-gray-900 mb-4">Create a franchise account!</h1>
          </div>

<form method="POST" action="{{ route('franchise.create') }}" enctype="multipart/form-data">
    {{csrf_field()}}

    <!-- 1st row -->

         <div class="row">
          <div class="col-md-6">
            <div>
            <input class="form-control my-2 form-control-user" id="name" name="name" type="text" value="{{ old('name') }}" placeholder="Enter name here" required>
            <p class="error-p">{{ $errors->first('name') }}</p>
            </div>

          </div>
          <!-- /.col -->
          <div class="col-md-6">
            <div>
            <input class="form-control my-2 form-control-user" id="email" name="email" type="email" value="{{ old('email') }}" placeholder="Enter your email here" required>
            <p class="error-p">{{ $errors->first('email') }}</p>
            </div>

          </div>
          <!-- /.col -->

         </div>
        <!-- /.row -->
<!-- 2nd row -->
        <div class="row">
          <div class="col-md-6">
                <div>
                <input class="form-control my-2 form-control-user" id="user_name" name="user_name" type="text" value="{{ old('user_name') }}" placeholder="User Name here" required>
                <p class="error-p">{{ $errors->first('user_name') }}</p>
                </div>

          </div>
          <!-- /.col -->
          <div class="col-md-6">
              <div>
              <input class="form-control my-2 form-control-user" id="password" name="password" type="password" placeholder="Enter password here" required>
              <p class="error-p">{{ $errors->first('password') }}</p>
              </div>

          </div>
          <!-- /.col -->

         </div>
        <!-- /.row -->

    <!-- 3rd row -->

    <div class="row">
          <div class="col-md-8">

          <div class="form-group">
           <textarea class="form-control my-2"  id="location" name="location" type="text" placeholder="Enter Address here" rows="3" required>{{ old('location') }}</textarea>
           <p class="error-p">{{ $errors->first('location') }}</p>
           </div>

          </div>
          <!-- /.col -->

          <div class="col-md-4">
          <div>
           <input class="form-control my-2 form-control-user" id="phone" name="phone" value="{{ old('phone') }}" type="number" placeholder="Enter Mobile Number here" required>
           <p class="error-p">{{ $errors->first('phone') }}</p>
          </div>

          <div>
           <input class="form-control my-2 form-control-user" id="agreement_date" name="agreement_date" type="date" value="{{ old('agreement_date') }}" placeholder="Enter Agreement Date here" data-toggle="tooltip" title ="Agreement Date" required>
           <p class="error-p">{{ $errors->first('agreement_date') }}</p>
          </div>



          </div>
          <!-- /.col -->


         </div>
        <!-- /.row -->


<!-- 4th row -->
        <div class="row">
          <div class="col-md-4">
          <div>
        <input class="form-control my-2 form-control-user" id="pf_esi" name="pf_esi" type="text" value="{{ old('pf_esi') }}" placeholder="Enter PF/ESI number here">
        <p class="error-p">{{ $errors->first('pf_esi') }}</p>
        </div>


          </div>
          <!-- /.col -->
          <div class="col-md-4">
          <div>
        <input class="form-control my-2 form-control-user" id="oficial_number" name="oficial_number" type="number" value="{{ old('oficial_number') }}" placeholder="Enter Official Mobile Number here" >
        <p class="error-p">{{ $errors->first('oficial_number') }}</p>
        </div>

          </div>
          <!-- /.col -->
          <div class="col-md-4">
          <div>
        <input class="form-control my-2 form-control-user" id="trade_license" name="trade_license" type="text" value="{{ old('trade_license') }}" placeholder="Enter Trade License here"  required>
        <p class="error-p">{{ $errors->first('trade_license') }}</p>
        </div>

          </div>
          <!-- /.col -->



         </div>
        <!-- /.row -->

<!-- 5th row -->
          <div class="row">
          <div class="col-md-8">
                  <div>
                  <input class="form-control my-2 form-control-user" id="franchise_account_manager" name="franchise_account_manager" type="text" value="{{ old('franchise_account_manager') }}" placeholder="Enter Franchise Account Manager's Name here" required>
                  <p class="error-p">{{ $errors->first('franchise_account_manager') }}</p>
                  </div>

          </div>
          <!-- /.col -->
          <div class="col-md-4">
                <div>
                  <input class="form-control my-2 form-control-user" id="ifsc_code" name="ifsc_code" type="text" value="{{ old('ifsc_code') }}" placeholder="Enter IFSC Code here" required>
                  <p class="error-p">{{ $errors->first('ifsc_code') }}</p>
                  </div>

          </div>
          <!-- /.col -->
          </div>

<!-- /.row -->

    <!-- 6th row -->

    <div class="row">
          <div class="col-md-12">
          <div>
        <input class="form-control my-2 form-control-user" id="bank_name" name="bank_name" type="text" value="{{ old('bank_name') }}" placeholder="Enter Bank Name here" required>
        <p class="error-p">{{ $errors->first('bank_name') }}</p>
        </div>

          </div>
          <!-- /.col -->
          </div>
<!-- /.row -->
     <div class="row">
          <div class="col-md-12">
          <div>
        <input class="form-control my-2 form-control-user" id="branch_name" name="branch_name" type="text" value="{{ old('branch_name') }}" placeholder="Enter Branch Name here" required>
        <p class="error-p">{{ $errors->first('branch_name') }}</p>
        </div>


          </div>
          <!-- /.col -->

         </div>
        <!-- /.row -->

<!-- 7th row -->
<div class="row">
          <div class="col-md-4">
          <div>
        <input class="form-control my-2 form-control-user" id="pan" name="pan" type="text" value="{{ old('pan') }}" placeholder="Enter PAN Number here" required>
        <p class="error-p">{{ $errors->first('pan') }}</p>
        </div>
          </div>
          <!-- /.col -->
          <div class="col-md-4">
          <div>
        <input class="form-control my-2 form-control-user" id="tan" name="tan" type="text" value="{{ old('tan') }}" placeholder="Enter TAN Number here">
        <p class="error-p">{{ $errors->first('tan') }}</p>
        </div>

          </div>
          <!-- /.col -->
          <div class="col-md-4">
            <div>
          <input class="form-control my-2 form-control-user" id="gst" name="gst" type="text" value="{{ old('gst') }}" placeholder="Enter GST Number here">
          <p class="error-p">{{ $errors->first('gst') }}</p>
          </div>

            </div>
          <!-- /.col -->



         </div>
        <!-- /.row -->
        <!-- Upload PAN, Adhar or Trade Licence -->

           <div class="row">
              <div class="col-md-6 mb-2">
                <!-- Custom bootstrap upload file-->



          <!-- End -->


           <div>
               <input type="file" value="{{ old('documents[]') }}" name="documents[]" multiple required>
           </div>
           <div>
                <p class="error-p">{{ $errors->first('documents') }}</p>
           </div>
        </div>
    </div>
    {{-- /.row --}}


        <div>
        <button type="submit" class="btn btn-primary ">Submit</button>
        </div>

</form>
<!-- register form end -->
</div>
</div>
      </div>
    </div>
  </div>
</div>

</div>


@endsection
