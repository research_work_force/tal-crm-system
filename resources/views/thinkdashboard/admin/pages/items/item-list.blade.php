@extends('thinkdashboard.admin.layouts.dashboard-layout')
@section('content')

 <!-- Begin Page Content -->
        <div class="container-fluid">


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Inventory items</h6>
             
            
            </div>
            
 <div class="card-body">
            <div class="row">
            
              <div class="col-md-2">
             </div>
              <div class="col-md-4">
                <form method="POST" action="{{route('inventory.item.showByFranchise')}}" >
                  {{ csrf_field() }}
                  <p><label>Search item by:</label>
              <select name="franchiseName" class="form-control my-2 form-control-user" required="" >
                <option selected disabled>---Select a franchise---</option>
                  @foreach($franchise_data as $f_data)
                  <option value="{{$f_data->user_name}}">{{ $f_data->name }}</option>
                    @endforeach
                </select>
                <input type="radio" name="itype" value="funtioning" checked>Functioning items<br>
                <input type="radio" name="itype" value="rejected">Rejected items<br>
                </p>
                <button class="btn btn-sm btn-info" type="submit">Go</button>
                </form>
              </div>

          </div>


                <p>Select franchise and item type first</p> 
 
              
            </div>
           

          </div>
  
          
        </div>
@endsection
