@extends('thinkdashboard.admin.layouts.dashboard-layout')
@section('content')

 <!-- Begin Page Content -->
        <div class="container-fluid">


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Inventory items</h6>
             
            
            </div>
            
 <div class="card-body">
            <div class="row">
            
              <div class="col-md-2">
             </div>
              <div class="col-md-4">
                
              </div>

          </div>
 
              <div class="table-responsive">

                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  <tr>
                    <th>Item</th>
                    <th>Category</th>
                    <th>Number of Items</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>Franchise</th>
                  </tr>
                  </thead>

                  <tbody>

                  @foreach($item_data as $item)
                  <tr>
                      <td>{{$item->item_name}}</td>
                      <td>{{$item->category_name}}</td>
                      <td>{{$item->number_of_items}}</td>
                      <td>{{$item->amount}}</td>
                      <td>{{$item->description}}</td>
                      <td>{{$item->added_by}}</td>
                      
                    </tr>
                    @endforeach

                    
                 </tbody>
                </table>
                
          
              </div>
            </div>
           <nav aria-label="Page navigation example">
                  {{$item_data->render("pagination::bootstrap-4") }}
               </nav>

          </div>
  
          
        </div>
@endsection
