@extends('thinkdashboard.admin.layouts.dashboard-layout')
@section('content')






        <!-- Begin Page Content -->
        <div class="container-fluid">


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">User Table</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>User Name</th>
                    <th>Phone Number</th>
                    <th>Location</th>
                    <th>Agreement Date</th>
                    <th>PF/ESI</th>
                    <th>Official Number</th>
                    <th>Franchise Account Managers</th>
                    <th>Trade Licence</th>
                    <th>IFSC Code</th>
                    <th>Bank Name</th>
                    <th>Branch Name</th>
                    <th>PAN</th>
                    <th>TAN</th>
                    <th>GST</th>
                    <th>Documents</th>
                    <th>Action</th>
                  </tr>
                  </thead>

                  <tbody>
                  @foreach($users as $user)
                  <tr>
                      <td>{{$user->name}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->u_name}}</td>
                      <td>{{$user->phone}}</td>
                      <td>{{$user->location}}</td>
                      <td>{{$user->ag_date}}</td>
                      <td>{{$user->pf_esi}}</td>
                      <td>{{$user->of_number}}</td>
                      <td>{{$user->fa_manager}}</td>
                      <td>{{$user->trd_license}}</td>
                      <td>{{$user->ifsc_code}}</td>
                      <td>{{$user->bank_name}}</td>
                      <td>{{$user->branch_name}}</td>
                      <td>{{$user->pan}}</td>
                      <td>{{$user->tan}}</td>
                      <td>{{$user->gst}}</td>
                      <td>
                        @php
                            $data = $user->documents;
                            list($pub,$upload,$documents,$year,$month,$name)=explode("/",$data);
                            $docs = "storage/".$upload."/".$documents."/".$year."/".$month."/".$name;
                        @endphp
                        <a class="btn btn-sm btn-info" href="{{ asset($docs) }}">View</a></td>
                      <td>
                            <!-- Edit delete -->
                         <div class="d-flex flex-row">
                               <div class="edit">
                                <a class = "btn btn-sm btn-info text-white mr-2" type="submit" data-toggle="modal" data-target="#modal-edit{{$user->franchise_id}}">edit</a>
                                @include('thinkdashboard.admin.include.editform',['users'=>$user])

                               </div>
                                <div class="delete">
                                    <a type="submit" class = "btn btn-sm btn-danger text-white"  data-toggle="modal" data-target="#modal-delete{{$user->franchise_id}}">delete</a>
                                    @include('thinkdashboard.admin.include.deletemodal',['id'=>$user->franchise_id])
                                </div>
                         </div>
                    </td>
                    </tr>
                    @endforeach
                 </tbody>
                </table>
              </div>
            </div>
           <nav aria-label="Page navigation example">
                  <ul class="pagination justify-content-end pull-right">
                    <li class="page-item disabled">
                      <a class="page-link" href="#" tabindex="-1">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#">Next</a>
                    </li>
                  </ul>
               </nav>
          </div>
        </div>
@endsection
