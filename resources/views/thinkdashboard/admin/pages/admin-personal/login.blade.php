@extends('thinkdashboard.admin.layouts.login-layout')
@section('content')
 

<div class="container">

<!-- Outer Row -->
<div class="row justify-content-center tal-row">

  <div class="col-xl-10 col-lg-12 col-md-9">

    <div class="card o-hidden border-0 shadow-lg my-5 tal-card">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5"><img src="{{asset('thinkdashboard/assets/img/logo/tal-logo1.png')}}" style="
    height: 143px; width: 377px; padding-left: 15px; padding-top: 12px; margin-top: 66px;"></div>
          <hr class="vl">
          <div class="col-lg-7">
            <div class="p-4">
              <div class="text-center">

                <!-- flash massage show -->
                @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
                @endif

                <h1 class="h4 text-gray-900 mb-4">Welcome Back Administrator!</h1>
              </div>
              <form class="user" method="POST" action="{{ route('admin.login') }}">
            {{csrf_field()}}
                <div class="form-group">
                  <input type="email" class="form-control form-control-user" name="email" value="{{ old('email') }}" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                  <p style="color:red;">{{ $errors->first('email') }}</p>
                  
                    
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-user" name="password" id="exampleInputPassword" placeholder="Password">
                  <p style="color:red;">{{ $errors->first('password') }}</p>
                </div>
                <div class="form-group">
                  
                </div>
                <div>
                <button type="submit" class="btn btn-primary btn-user btn-block tal-btn">Sign-up</button>
                </div>

              </form>
              

            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

</div>

</div>



@endsection
