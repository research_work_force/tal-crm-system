@extends('thinkdashboard.admin.layouts.dashboard-layout')
@section('content')

<div class="container-fluid content-wrapper ">
    <div class="card pb-4">

        <div class="content row think-user-profile">
          <div class="col-md-4">
              {{-- <div class="card think-card think-user-info px-4"> --}}


                          <div class="user-img text-center  mt-3 pt-1">
                          <img class="img-fluid mt-1 img-rounded" src="{{asset('thinkdashboard\assets\img\user\user1.jpg')}}" alt="">

                          </div>
                          <div class="d-flex flex-row justify-content-center mt-1">
                              <div class="edit-btn text-center mt-4">
                                  <a href="#" class="btn btn-secondary btn-sm">Edit profile</a>
                              </div>
                          </div>



                      {{-- /.flex --}}



                      {{-- <div class="d-flex flex-row justify-content-between ">
                             <div class="followers">
                                 <p>Files</p>
                                 <p class="success">123</p>
                             </div>
                             <div class="followers">
                                 <p>Files</p>
                                 <p class="success">123</p>

                             </div>
                             <div class="followers">
                                 <p>Files</p>
                                 <p class="success">123</p>

                             </div>
                      </div> --}}

                 <!-- user info -->

                   {{-- </div> --}}
                   {{-- /. card --}}

                   {{-- <div class="card think-card px-4 my-2">
                       <h6 class="mt-2">New Tasks Assigned</h6>
                       <hr>
                       <div class="tasks d-flex flex-row">

                           <div class="pr-2">
                                <i class="fas fa-arrow-alt-circle-right"></i>

                           </div>
                           <div class="">
                                <p>Lorem ipsum dolor sit amet consectetur.</p>

                           </div>
                       </div>
                       <div class="tasks d-flex flex-row">

                            <div class="pr-2">
                                 <i class="fas fa-arrow-alt-circle-right"></i>

                            </div>
                            <div class="">
                                 <p>Lorem ipsum dolor sit amet consectetur.</p>

                            </div>
                        </div>
                        <div class="tasks d-flex flex-row">

                                <div class="pr-2">
                                     <i class="fas fa-arrow-alt-circle-right"></i>

                                </div>
                                <div class="">
                                     <p>Lorem ipsum dolor sit amet consectetur.</p>

                                </div>
                            </div>
                            <div class="tasks d-flex flex-row">

                                    <div class="pr-2">
                                         <i class="fas fa-arrow-alt-circle-right"></i>

                                    </div>
                                    <div class="">
                                         <p>Lorem ipsum dolor sit amet consectetur.</p>

                                    </div>
                                </div>
                                <div class="tasks d-flex flex-row">

                                        <div class="pr-2">
                                             <i class="fas fa-arrow-alt-circle-right"></i>

                                        </div>
                                        <div class="">
                                             <p>Lorem ipsum dolor sit amet consectetur.</p>

                                        </div>
                                    </div>

                   </div> --}}


          </div>
          <!-- /.col -->


          <div class="col-md-7">
                <div class="mt-4">
                        <div class="follow ">

                                <div class="user-information text-left">
                                  <div class="clearfix">
                                      <div class="float-left">
                                          <div class="d-flex flex-row justify-content-start">
                                              <div class="mr-4">
                                                    <h6>Rituporno Ghosh</h6>

                                              </div>
                                              <div>
                                                    <p>                                                <i class="fas fa-map-marker-alt"></i>
                                                        Kolkata
                                                    </p>

                                              </div>

                                          </div>
                                      </div>
                                      <div class="float-right">

                                      </div>
                                  </div>
                                 <p class="success">Actor</p>
                                </div>
                        </div>

                        <h6 class="mt-4">Qualification</h6>

                        <hr>
                        <p class="success">Education</p>

                        <p class="mb-2">B.S. in Computer Science from the University of Tennessee at Knoxville</p>

                        {{-- <p class="success">Location</p>

                        <p>Kolkata, India</p>
                        <hr> --}}
                        <p class="success ">Skills</p>
                        <div class="d-flex flex-row">
                            <div class="mr-2">
                                   <span class="badge badge-danger">Ai</span>

                            </div>
                            <div class="mr-2">
                                   <span class="badge badge-warning">Robotics</span>

                            </div>
                            <div class="mr-2">
                                   <span class="badge badge-info">Backend</span>

                            </div>

                        </div>


                    </div>
                    {{-- /.p-4 --}}




          </div>
          <!-- /.col -->

        </div>
         <!-- /.row -->
    </div>
    {{-- /.card --}}


</div>
<!-- /.container -->







@endsection
