@extends('thinkdashboard.admin.layouts.login-layout')
@section('content')


<div class="container">

<div class="card o-hidden border-0 shadow-lg my-5">
  <div class="card-body p-0">
    <!-- Nested Row within Card Body -->
    <div class="row">
      <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
      <div class="col-lg-7">
        <div class="p-5">
          <div class="text-center">

            <!-- flash massage show -->
            @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif

            <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
          </div>
          <form class="user"  method="POST" action="{{ route('admin.register') }}">
            {{csrf_field()}}
            <div class="form-group row">
              <div class="col-sm-6 mb-3 mb-sm-0">
                <input type="text" class="form-control form-control-user" name="firstname" id="exampleFirstName" placeholder="First Name" value="{{ old('firstname') }}">
                    <p style="color:red;">{{ $errors->first('firstname') }}</p>
            </div>
              <div class="col-sm-6">
                <input type="text" class="form-control form-control-user" name="lastname" id="exampleLastName" placeholder="Last Name" value="{{ old('lastname') }}">
                <p style="color:red;">{{ $errors->first('lastname') }}</p>
              </div>
            </div>
            <div class="form-group">
              <input type="email" class="form-control form-control-user" name="email" id="exampleInputEmail" placeholder="Email Address" value="{{ old('email') }}">
              <p style="color:red;">{{ $errors->first('email') }}</p>
            </div>
            <div class="form-group">
              <input type="text" class="form-control form-control-user" name="address" id="exampleInputAddress" placeholder="Address" value="{{ old('address') }}">
              <p style="color:red;">{{ $errors->first('address') }}</p>
            </div>
            <div class="form-group">
              <input type="number" class="form-control form-control-user" name="contact" id="exampleInputNumber" placeholder="Contact Number" value="{{ old('contact') }}">
              <p style="color:red;">{{ $errors->first('contact') }}</p>
            </div>
            <div class="form-group row">
              <div class="col-sm-6 mb-3 mb-sm-0">
                <input type="password" class="form-control form-control-user" name="password"  id="exampleInputPassword" placeholder="Password">
                <p style="color:red;">{{ $errors->first('password') }}</p>
              </div>
              <div class="col-sm-6">
                <input type="password" class="form-control form-control-user" name="password_confirmation" id="exampleRepeatPassword" placeholder="Repeat Password">
              </div>
            </div>
        <div>
        <button type="submit" class="btn btn-primary btn-user btn-block">Sign Up</button>
        </div>

          </form>
          <hr>

          <div class="text-center">
              Already have an account?<a href="/admin/login"> Login!</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
@endsection
