@extends('thinkdashboard.admin.layouts.login-layout')
@section('content')

	<!-- flash massage show -->
            @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif

	<form>
				<div class="form-group">
					<p>Forgot your password,Enter your email id to get password reset link</p><br>
                  <input type="email" class="form-control form-control-user" name="email" value="{{ old('email') }}" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address">
                  <p style="color:red;">{{ $errors->first('email') }}</p>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Submit</button>
        </div>
	</form>
