@extends('thinkdashboard.admin.layouts.dashboard-layout')
@section('content')

<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
         <div class="row">
            <div class="col-md-12">
            <div class="container">
            <div class="p-5">
                <div class="text-center">
                    <h4> Create course</h4>
                </div>
                <div class="clearfix sticky-top">
                    <div class="float-right">
                            <div class="bulk ">
                                    <!-- <a class = "btn btn-info text-white"  data-toggle="modal" data-target="#modal-bulk">Bulk Upload</a> -->
                                    <a class = "btn btn-info text-white" href="/admin/course/list">Show list</a>                                  

                            </div>
                    </div>
                </div>
                @include('thinkdashboard.admin.include.modal.bulk-modal')

              <form method="POST" action="{{route('create.course')}}" enctype="multipart/form-data">
               {{csrf_field()}}

            <!-- 1st row -->

                 <div class="row">
                  <div class="col-md-6">
                    <div>
                    <input class="form-control my-2 form-control-user" id="name" name="course_name" type="text" value="{{ old('course_name') }}" placeholder="Enter Course name ">
                    <p style="color:red;">{{ $errors->first('course_name') }}</p>
                    </div>

                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                    <div>
                    <input class="form-control my-2 form-control-user" id="course_code" name="course_code" type="text" value="{{ old('course_code') }}" placeholder="Enter corse code here">
                    <p style="color:red;">{{ $errors->first('course_code') }}</p>
                    </div>

                  </div>
                  <!-- /.col -->

                 </div>
                <!-- /.row -->


                <div class="row">
                     <div class="col-md-6">
                            <textarea maxlength="150" class="form-control py-3 form-control-user" id="course_description" name="course_description" type="text" placeholder="Enter course description here (150 characters)">{{ old('course_description') }}</textarea>
                             <p style="color:red;">{{ $errors->first('course_description') }}</p>
                     </div>
                     {{-- /.col --}}
                     <div class="col-md-6">
                            <input class="form-control form-control-user" id="course_tags" name="course_tags" type="text" value="{{ old('course_tags') }}" placeholder="Enter Tags Separate With Comma(,)">
                             <p style="color:red;">{{ $errors->first('course_tags') }}</p>


                             <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="thumbnail" value="{{ old('thumbnail') }}">
                                    <label class="custom-file-label" for="customFile">Choose image</label>
                            </div>
                            <p style="color:red;">{{ $errors->first('thumbnail') }}</p>

                     </div>
                     {{-- /.col --}}


                </div>
                {{-- /.row --}}
        <!-- 2nd row -->
                <div class="row">
                  <div class="col-md-6">
                        <div>
                        <input class="form-control my-2 form-control-user" id="course_duration" name="course_duration" type="number" value="{{ old('course_duration') }}" placeholder="User course duration in Hour">
                        <p style="color:red;">{{ $errors->first('course_duration') }}</p>
                        </div>

                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                      <div>
                      <input class="form-control my-2 form-control-user" id="course_fees" name="course_fees" type="number" value="{{ old('course_fees') }}" type="course_fees" placeholder="Enter coruse fees here">
                      <p style="color:red;">{{ $errors->first('course_fees') }}</p>
                      </div>

                  </div>
                  <!-- /.col -->

                 </div>
                <!-- /.row -->

            <!-- 3rd row -->

            <div class="row">
                  <div class="col-md-5">
                    </div>
                      <!-- /.col -->

                  <div class="col-md-4">



                  </div>
                  <!-- /.col -->

            </div>
            {{-- /.row --}}




                <div>
                <button type="submit" class="btn btn-primary ">Submit</button>
                </div>

          </form>
        </div>
        </div>
          </div>
        </div>
      </div>
    </div>
</div>



@endsection
