@extends('thinkdashboard.admin.layouts.dashboard-layout')
@section('content')

<div class="container">


    <div class="row">
        @foreach ($course as $item)

        <div class="col-md-4 my-3">
                <div class="card card-shadow">
                    <div class="zoom-img-holder">
                        <img  class="card-img-top img-dark-zoom" src="{{asset('upload/course/thumbnail/'.$item->thumbnail)}}" alt="">
                    </div>
                    <div class="card-body">
                        <p class="bolder color-text">{{ $item->course_name }}({{ $item->course_code }})</p>
                         <p class="small-p course_description">{{ str_limit($item->course_description, 155) }}</p>
                        <div class="clearfix">
                            <div class="float-left">
                                <p class="small-p-bold"><i class="fas fa-rupee-sign"></i>{{ $item->course_fees }}</p>

                            </div>
                            <div class="float-right">
                                <p class="small-p-bold">{{ $item->course_duration }} Hours</p>

                            </div>

                        </div>
                        {{-- /.clearfix --}}

                        <div class="clearfix">
                                <div class="float-left">

                                @if($item->status == 1)
                                <a href="/admin/course/block/{{ $item->course_code }}" class = "btn btn-sm btn-danger text-white mr-2" >Block</a>
                              @elseif($item->status == 0)
                                <a href="/admin/course/block/{{ $item->course_code }}" class = "btn btn-sm btn-success text-white mr-2" >Unblock</a>
                                @endif

                                <a href="/admin/course/edit/{{ $item->id }}" class = "btn btn-sm btn-warning text-white mr-2" >Edit</a>
                               
                               <a href="/admin/course/delete/{{ $item->id }}" class = "btn btn-sm btn-danger text-white mr-2" >Delete</a>

                                </div>
                                <div class="float-right">
                                   

                                </div>
                           

                        </div>
                        {{-- /.clearfix --}}

                              <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-8">
                                  <div class="float-right">
                                 @php
                                        $tags_array = explode(",", $item->course_tags);
                                    @endphp

                                    @foreach ($tags_array as $tags)

                                    <span href="#" class="btn  m-0 p-0"><span class="badge badge-secondary">{{ $tags }}</span></span>

                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach


</div>

{{ $course->render("pagination::bootstrap-4") }}

@endsection

