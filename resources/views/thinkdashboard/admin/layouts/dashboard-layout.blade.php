<!DOCTYPE html>
<html lang="en">
<head>
      @include('thinkdashboard.admin.include.links.headerlinks')
      <title>Admin(Beta)</title>
</head>
<body id="page-top">
   <div id="wrapper">


     @include('thinkdashboard.admin.include.constants.mainsidebar')

      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">


      <!-- Main Content -->
      <div id="content">

      @include('thinkdashboard.admin.include.constants.navbar')

      <!-- flash massage show -->
      @if(Session::has('message'))
      <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
      @endif

      @yield('content')

     </div>
     <!-- content end -->
     @include('thinkdashboard.admin.include.constants.footer')

     </div>
     <!-- main content end -->
     </div>

     @include('thinkdashboard.admin.include.links.footerlinks')

</body>
</html>

