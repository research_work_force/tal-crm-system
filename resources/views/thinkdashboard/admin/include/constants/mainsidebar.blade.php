<!-- Sidebar -->
<ul class="navbar-nav sidebar-background-color sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="tal-brand">
  <div class="sidebar-brand-icon ">
    <img src="{{asset('thinkdashboard/assets/img/logo/tal-logo.png')}} " alt="">
  </div>
  
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->
<!-- <li class="nav-item active">
  <a class="nav-link" href="/admin/dashboard">
    <i class="fas fa-fw fa-tachometer-alt"></i>
    <span>Dashboard</span></a>
</li> -->

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
{{-- <div class="sidebar-heading">
  Interface
</div> --}}

<?php 
function activeMenu($uri = '') {
    $active = '';
    if (Request::is(Request::segment(1) . '/' . $uri . '/*') || Request::is(Request::segment(1) . '/' . $uri) || Request::is($uri)) {
        $active = 'active';
    }
    return $active;
}
?>

<li class="nav-item">
    <a href="/admin/register" class="nav-link">
        <i class="fas fa-user-friends text-white"></i>
        <span>Admin Registration</span>

    </a>

</li>



<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item {{ activeMenu('franchise') }}">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
    <i class="fas fa-user-friends text-white"></i>
    <span>Franchise</span>
  </a>
  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      {{-- <h6 class="collapse-header"></h6> --}}
      <a class="collapse-item" href="/admin/franchise/create">Create</a>
      <a class="collapse-item" href="/admin/franchise/list">List</a>
    </div>
  </div>
</li>

<!-- Nav Item - Utilities Collapse Menu -->
<li class="nav-item {{ activeMenu('course') }}">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
    <i class="fas text-white fa-graduation-cap"></i>
    <span>Courses</span>
  </a>
  <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="/admin/course/create">Create</a>
      <a class="collapse-item" href="/admin/course/list">List</a>
    </div>
  </div>
</li>

<!-- Nav Item - Pages Collapse Menu -->
{{-- Coupon --}}
<li class="nav-item {{ activeMenu('coupon') }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#sagar" aria-expanded="true" aria-controls="sagar">
          <i class="fas text-white fa-gift"></i>
          <span>Coupon</span>
        </a>
        <div id="sagar" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="/admin/coupon/create">Create</a>
            <a class="collapse-item dropdown-toggle" data-toggle="dropdown">List</a>

                    <div class="dropdown-menu" >
                        <a class="dropdown-item" href="/admin/coupon/list/view/categories">Categories</a>
                        <a class="dropdown-item" href="/admin/coupon/list/non-categories">Non Categories</a>
                    </div>



          </div>
        </div>
      </li>
{{-- Coupon --}}

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item {{ activeMenu('student') }}">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#studentList" aria-expanded="true" aria-controls="studentList">
    <i class="fas fa-user-friends text-white"></i>
    <span>Students</span>
  </a>
  <div id="studentList" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      {{-- <h6 class="collapse-header"></h6> --}}

      <a class="collapse-item" href="{{route('students.list')}}">View</a>
    </div>
  </div>
</li>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item {{ activeMenu('inventory') }}">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#inventorycollapse" aria-expanded="true" aria-controls="inventorycollapse">
    
    <i class="fa fa-list text-white" aria-hidden="true"></i>
    <span>Inventory</span>
  </a>
  <div id="inventorycollapse" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
   
      {{-- <h6 class="collapse-header"></h6> --}}

      <a class="collapse-item" href="{{route('inventory.item.showAll')}}">Item List</a>
    </div>
  </div>


</li>



<!-- Heading -->



<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<!-- <div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div> -->

</ul>
<!-- End of Sidebar -->
