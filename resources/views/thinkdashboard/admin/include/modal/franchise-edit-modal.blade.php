<!-- edit modal -->

<div class="modal fade" id="modal-edit{{$users->franchise_id}}" tabindex="-1" role="dialog" >
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit your data</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            
            </div>
    <div class="modal-body">

<form method="POST" action="{{route('franchise.update', $users->user_name)}}" enctype="multipart/form-data">
    {{csrf_field()}}

    <div>
        <input id="franchise_id" name="franchise_id" type="hidden" value= "{{$users->franchise_id}}">
    </div>

    <div class="form-group has-feedback">
    <input id="name" name="name" type="text" value= "{{$users->name}}"  class="form-control"  placeholder="Name">
    </div>

    <div class="form-group has-feedback">
    <input id="email" name="email"  value= "{{$users->email}}"  type="email" class="form-control" placeholder="Email">
    </div>

    <div class="form-group has-feedback">
    <input id="user_name" name="user_name"  value= "{{$users->user_name}}" type="text" class="form-control" placeholder="Username" readonly>
    </div>

    <div class="form-group has-feedback">
    <input id="phone" name="phone"  value= "{{$users->phone}}" type="number" class="form-control" placeholder="Phone Number">
    </div>

    <div class="form-group has-feedback">
    <input id="location" name="location"  value= "{{$users->location}}" type="text" class="form-control" placeholder="Location">
    </div>

    <div class="form-group has-feedback">
    <input id="agreement_date" name="agreement_date"  value= "{{$users->agreement_date}}" type="date" class="form-control" placeholder="Agreement Date">
    </div>

    <div class="form-group has-feedback">
    <input id="pf_esi" name="pf_esi"  value= "{{$users->pf_esi}}" type="text" class="form-control" placeholder="PF/ESI">
    </div>

    <div class="form-group has-feedback">
    <input id="oficial_number" name="oficial_number"  value= "{{$users->oficial_number}}" type="number" class="form-control" placeholder="Official Number">
    </div>

    <div class="form-group has-feedback">
    <input id="franchise_account_manager" name="franchise_account_manager"  value= "{{$users->franchise_account_manager}}" type="text" class="form-control" placeholder="Franchise Account Managers">
    </div>

    <div class="form-group has-feedback">
    <input id="trade_license" name="trade_license"  value= "{{$users->trade_license}}" type="text" class="form-control" placeholder="Trade License">
    </div>

    <div class="form-group has-feedback">
    <input id="ifsc_code" name="ifsc_code"  value= "{{$users->ifsc_code}}" type="text" class="form-control" placeholder="IFSC Code">
    </div>

    <div class="form-group has-feedback">
    <input id="bank_name" name="bank_name"  value= "{{$users->bank_name}}" type="text" class="form-control" placeholder="Bank Name">
    </div>

    <div class="form-group has-feedback">
    <input id="branch_name" name="branch_name"  value= "{{$users->branch_name}}" type="text" class="form-control" placeholder="Branch Name">
    </div>

    <div class="form-group has-feedback">
    <input id="pan" name="pan"  value= "{{$users->pan}}" type="text" class="form-control" placeholder="PAN">
    </div>

    <div class="form-group has-feedback">
    <input id="tan" name="tan"  value= "{{$users->tan}}" type="text" class="form-control" placeholder="TAN">
    </div>

    <div class="form-group has-feedback">
    <input id="gst" name="gst"  value= "{{$users->gst}}" type="text" class="form-control" placeholder="GST">
    </div>

  </div>
        <!-- /.modal-body -->

        <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </div>

</form>
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- /.edit modal -->


