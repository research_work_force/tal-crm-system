<!-- edit modal -->

<div class="modal fade" id="modal-edit{{ $users->coupon_code }}" tabindex="-1" role="dialog" >
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit your data</h4>
            </div>
    <div class="modal-body">

<form method="POST" action="{{route('coupon.update')}}" >
    @csrf
<input type="hidden" name="course_id" value="{{$users->id}}">
    <div class="form-group has-feedback">
    <input id="coupon_code" name="coupon_code" type="text" value= "{{$users->coupon_code}}"  class="form-control"  placeholder="code">
    </div>

    <div class="form-group has-feedback">
    <input id="coupon_description" name="coupon_description"  value= "{{$users->coupon_description}}"  type="coupon_description" class="form-control" placeholder="description">
    </div>

    <div class="form-group has-feedback">
    <input id="discount_percentage" name="discount_percentage"  value= "{{ $users->discount_percentage }}" type="number" class="form-control" placeholder="Percentage">
    </div>


                                    <div class="form-group">
                                                <select class="form-control"  name="course_code" disabled>
                                                  <option value="{{ $users->course_code }}" selected >{{ $users->course_name }}</option>
                                                @foreach ($course as $courses)
                                                <option value="{{$courses->course_code}}">{{$courses->course_name}}</option>
                                                @endforeach
                                                </select>
                                                <p style="color:red;">{{ $errors->first('course_code') }}</p>

                                    </div>


                                    <div class="form-group">
                                                <select class="form-control"  name="franchise_user_name" disabled>
                                                  <option value="{{ $users->user_name }}" selected >{{ $users->name }}</option>
                                                  @foreach ($fran as $franc)
                                                  <option value="{{$franc->user_name}}">{{$franc->name}}</option>
                                                  @endforeach
                                                </select>
                                                <p style="color:red;">{{ $errors->first('franchise_user_name') }}</p>

                                    </div>


        </div>


        <!-- /.modal-body -->

        <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </div>

</form>
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- /.edit modal -->


