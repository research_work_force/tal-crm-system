<!-- delete Modal -->
<div class="modal fade" id="modal-delete{{$id}}">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete</h4>
            </div>
            <div class="modal-body">
            <p>Are you sure?</p>
            </div>
            <div class="modal-footer">
            <a  href="#" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</a>
            <a href="/delete-prof{{$id}}" type="submit" class="btn btn-primary">confirm</a>
            </div>
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<!-- /. delete modal -->
