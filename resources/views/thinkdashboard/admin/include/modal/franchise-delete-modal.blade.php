<!-- delete Modal -->
<div class="modal fade" id="modal-f-delete{{$user_name}}">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            
            </div>
            <div class="modal-body">
            <p>Are you sure?</p>
            </div>
            <div class="modal-footer">
            <a  href="#" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</a>
            <a href="/admin/franchise/delete/{{$user_name}}" type="submit" class="btn btn-primary">confirm</a>
            </div>
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -- >
    </div>
    <!-- /.modal -->
<!-- /. delete modal -->
