<!-- Bulk Modal -->
<div class="modal fade" id="modal-bulk"  tabindex="-1" role="dialog" >
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
            </div>
             <div class="modal-body">
                <form method="POST" action="{{route('import.course')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="row">
                            <div class="col-md-12">
                                    <div class="custom-file">
                                            <input type="file" name="file" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                            </div>

                    </div>

            </div>
            {{-- /.modal body --}}

                     <div class="modal-footer">
                             <a  class="btn btn-danger pull-left text-white" data-dismiss="modal">Cancel</a>
                            <a type="submit" class="btn btn-primary text-white">confirm</a>
                     </div>
                </form>

        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<!-- /. delete modal -->
