<!DOCTYPE html>
<html lang="en">
<head>
      @include('thinkdashboard.tic.include.headerlinks')
      <title>Admin</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">

     @include('thinkdashboard.tic.include.navbar')

     @include('thinkdashboard.tic.include.mainsidebar')

     @yield('content')
     
     @include('thinkdashboard.tic.include.footer')
    
     </div>

     @include('thinkdashboard.tic.include.footerlinks')
  
</body>
</html>