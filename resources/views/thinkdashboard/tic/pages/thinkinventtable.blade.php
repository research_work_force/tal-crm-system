@extends('thinkdashboard.tic.layouts.ticlayout')
@section('content')


<div class="container-fluid content-wrapper">

      <div class="row content">
         <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">Franchise</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>User Name</th>
                  <th>Scool Name</th>
                  <th>Contact details</th>
                  <th>Address</th>
                  <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>Misc</td>
                  <td>Dillo 0.8</td>
                  <td>Embedded devices</td>
                  <td>-</td>
                  <td>
                  <!-- Edit delete -->
                  <a href="#" class = "btn btn-sm btn-primary"  data-toggle="modal" data-target="#modal-edit">edit</a>
                  <a href="#" class = "btn btn-sm btn-danger"  data-toggle="modal" data-target="#modal-delete">delete</a>
                  </td>
                </tr>
                <tr>
                  <td>Misc</td>
                  <td>Links</td>
                  <td>Text only</td>
                  <td>-</td>
                  <td>
                  <!-- Edit delete -->
                  <a href="#" class = "btn btn-sm btn-primary"  data-toggle="modal" data-target="#modal-edit">edit</a>
                  <a href="#" class = "btn btn-sm btn-danger"  data-toggle="modal" data-target="#modal-delete">delete</a>
                  </td>
                </tr>
                <tr>
                  <td>Misc</td>
                  <td>Lynx</td>
                  <td>Text only</td>
                  <td>-</td>
                  <td>
                  <!-- Edit delete -->
                  <a href="#" class = "btn btn-sm btn-primary"  data-toggle="modal" data-target="#modal-edit">edit</a>
                  <a href="#" class = "btn btn-sm btn-danger"  data-toggle="modal" data-target="#modal-delete">delete</a>
                  </td>
                </tr>
                <tr>
                  <td>Misc</td>
                  <td>IE Mobile</td>
                  <td>Windows Mobile 6</td>
                  <td>-</td>
                  <td>
                  <!-- Edit delete -->
                  <a href="#" class = "btn btn-sm btn-primary"  data-toggle="modal" data-target="#modal-edit">edit</a>
                  <a href="#" class = "btn btn-sm btn-danger"  data-toggle="modal" data-target="#modal-delete">delete</a>
                  </td>
                </tr>
                <tr>
                  <td>Misc</td>
                  <td>PSP browser</td>
                  <td>PSP</td>
                  <td>-</td>
                  <td>
                  <!-- Edit delete -->
                  <a href="#" class = "btn btn-sm btn-primary"  data-toggle="modal" data-target="#modal-edit">edit</a>
                  <a href="#" class = "btn btn-sm btn-danger"  data-toggle="modal" data-target="#modal-delete">delete</a>
                  </td>
                </tr>
                <tr>
                  <td>Other browsers</td>
                  <td>All others</td>
                  <td>-</td>
                  <td>-</td>
                  <td>
                  <!-- Edit delete -->
                  <a href="#" class = "btn btn-sm btn-primary"  data-toggle="modal" data-target="#modal-edit">edit</a>
                  <a href="#" class = "btn btn-sm btn-danger"  data-toggle="modal" data-target="#modal-delete">delete</a>
                  </td>
                </tr>
                </tbody>
                
              </table>
              </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
           
              <nav aria-label="Page navigation example">
                  <ul class="pagination justify-content-end pull-right">
                    <li class="page-item disabled">
                      <a class="page-link" href="#" tabindex="-1">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#">Next</a>
                    </li>
                  </ul>
               </nav>
         </div>
           <!-- /.col -->
         
      </div>
      <!-- /.row -->

   </div>
<!-- /.conatiner -->


<!--------------------
     MODALs
-------------------->

<!-- edit modal -->

       <div class="modal fade" id="modal-edit">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit your data</h4>
              </div>
              <div class="modal-body">
              <form method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Full name">
        <!-- <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="School name">
        <!-- <span class="glyphicon glyphicon-envelope form-control-feedback"></span> -->
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Address">
        <!-- <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Contact Details">
        <!-- <span class="glyphicon glyphicon-envelope form-control-feedback"></span> -->
      </div>
      
    
    </form>
                        <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Full name">
                        <!-- <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
                        </div>
                        <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Address">
                        <!-- <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
                        </div>
                        <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Phone Number">
                        <!-- <span class="glyphicon glyphicon-envelope form-control-feedback"></span> -->
                        </div>
                        <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Email">
                        <!-- <span class="glyphicon glyphicon-envelope form-control-feedback"></span> -->
                        </div>
                        
                        
                        <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Location">
                        <!-- <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
                        </div>
                  
                        
                        
                  
                  </form>

              </div>
              <!-- /.modal-body -->

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


<!-- /.edit modal -->


<!-- ------------------- -->


<!-- delete Modal -->

       <div class="modal fade" id="modal-delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure? </p>
              </div>
              <div class="modal-footer">
                <a href = "#" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</a>
                <a href = "#" class="btn btn-primary">confirm</a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
<!-- /. delete modal -->

@endsection