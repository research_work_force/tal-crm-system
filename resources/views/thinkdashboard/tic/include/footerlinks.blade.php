<!-- jQuery 3 -->
<script src="{{asset('thinkdashboard/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('thinkdashboard/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('thinkdashboard/plugins/fastclick/fastclick.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('thinkdashboard/dist/js/adminlte.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('thinkdashboard/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>
<script src="{{asset('thinkdashboard/js/docs.js')}}"></script>