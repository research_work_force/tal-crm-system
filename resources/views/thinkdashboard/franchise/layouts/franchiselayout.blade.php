<!DOCTYPE html>
<html lang="en">
<head>
      @include('thinkdashboard.franchise.include.links.headerlinks')
      <title>Franchise</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">

     @include('thinkdashboard.franchise.include.constants.navbar')

     @include('thinkdashboard.franchise.include.constants.mainsidebar')

     @yield('content')

     @include('thinkdashboard.franchise.include.constants.footer')

     </div>

     @include('thinkdashboard.franchise.include.links.footerlinks')

</body>
</html>
