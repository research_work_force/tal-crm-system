<!DOCTYPE html>
<html lang="en">
<head>
      @include('thinkdashboard.franchise.include.links.headerlinks') 
      <title>Franchise</title>
</head>
<body id="page-top">
   <div id="wrapper">


     @include('thinkdashboard.franchise.include.constants.mainsidebar')

      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">


      <!-- Main Content -->
      <div id="content">

      @include('thinkdashboard.franchise.include.constants.navbar')

       <!-- flash massage show -->
      @if(Session::has('message'))
      <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
      @endif

             <!-- error handaler -->
        @if ($errors->any())
        <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>
        @endif

      @yield('content')


     </div>
     <!-- content end -->
     @include('thinkdashboard.franchise.include.constants.footer')

     </div>
     <!-- main content end -->
     </div>

     @include('thinkdashboard.franchise.include.links.footerlinks')

</body>
</html>

