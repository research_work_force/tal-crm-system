 <!-- delete Modal -->
<div class="modal fade" id="item-rejected-modal{{$code}}">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <h4 class="modal-title">Rejected Item</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
           
            </div>
            <div class="modal-body">
            

            <form method="POST" action="{{route('inventory.items.rejected')}}">
               {{csrf_field()}}
               <input type="hidden" name="id" value="{{ $code }}">

            <!-- 1st row -->
       
                 
            <div class="row">
              <div class="col-md-6">
                <input  class="form-control my-2 form-control-user" type="number" name="item_quantity" placeholder="Number of Item rejected"  required="">
              </div>

              <div class="col-md-6">
                <input  class="form-control my-2 form-control-user" type="text" name="remarks" placeholder="Remarks"  required="">
              </div>

            </div>
               
          
            </div>
            <div class="modal-footer">
            <a  href="#" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</a>
            <button type="submit" class="btn btn-primary">Update</button>
            </div>
             </form>
         
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<!-- /. delete modal -->
