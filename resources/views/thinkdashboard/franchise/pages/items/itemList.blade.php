@extends('thinkdashboard.franchise.layouts.dashboard-layout')
@section('content')

 <!-- Begin Page Content -->
        <div class="container-fluid">


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Items</h6>
             
            
            </div>
            <div class="card-body">
    @include('thinkdashboard.franchise.pages.items.CategoryAddModal')
              <form action="{{ route('inventory.items.create')}}" method="POST">
                {{ csrf_field() }}
              <div class="row">
                <div class="col-md-3">
                  <select class="form-control my-2 form-control-user" name="category_name" required="">
                    <option readonly>---Select a category---</option>
                    @foreach($categories_data as $cat_data)
                  <option value="{{$cat_data->category_name}}">{{ $cat_data->category_name }}</option>
                    @endforeach
                </select>
              </div>
                <div class="col-md-1">
                <a class = "btn btn-sm btn-primary " data-toggle="modal" data-target="#modal-category-add" href=""><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
             
              </div>
                <div class="col-md-4">
                <input  class="form-control my-2 form-control-user" type="text" name="item_name" placeholder="Item name" required="">
                </div>
                <div class="col-md-4">
                <input  class="form-control my-2 form-control-user" type="number" name="item_quantity" placeholder="Item quantity" required="">
              </div>
            </div>
              <div class="row">
              <div class="col-md-4">
                <input  class="form-control my-2 form-control-user" type="number" name="item_amount" placeholder="Amount" required="">
              </div>
              <div class="col-md-4">
                <input  class="form-control my-2 form-control-user" type="text" name="item_description" placeholder="Description" required="">
              </div>
              <div class="col-md-4">
                <button type="submit" class="btn btn-primary">Add item</button>
              </div>
            </div>
                
      
          </form>
                </div>
 <div class="card-body">
  <form method="POST" action="{{route('inventory.items.search')}}">
    {{csrf_field()}}
            <div class="row">
              <div class="col-md-2">
               <label>Search item by:</label>
             </div>
            
              <div class="col-md-4">
              <select class="form-control my-2 form-control-user" name="category_name">
                <option value="0" readonly>---Select a category---</option>
                  @foreach($categories_data as $cat_data)
                  <option value="{{$cat_data->category_name}}">{{ $cat_data->category_name }}</option>
                    @endforeach
                </select>
              </div>
              <div class="col-md-4">
              <input  class="form-control my-2 form-control-user" type="text" name="item_name" placeholder="Item name">
                
            </div>
            <div class="col-md-2">
              <button class="btn btn-primary">Search</button>
            </div>

          </div>
          </form>

              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  <tr>
                    <th>Item</th>
                    <th>Category</th>
                    <th>Number of Items</th>
                    <th>Price (&#8377) (per item)</th>
                    <th>Description</th>
                    <th>Action</th>
                  </tr>
                  </thead>

                  <tbody>
                  @foreach($item_data as $item)
                  <tr>
                      <td>{{$item->item_name}}</td>
                      <td>{{$item->category_name}}</td>
                      <td>{{$item->number_of_items}}</td>
                      <td>&#8377 {{$item->amount}}</td>
                      <td>{{$item->description}}</td>
                      <td> 
                        <a type="submit" class = "btn btn-sm btn-secondary text-white"  data-toggle="modal" data-target="#modal-item-edit{{$item->id}}">Edit</a>
                          @include('thinkdashboard.franchise.pages.items.itemEditModal',['code'=>$item->id,'item'=>$item])

                        <a type="submit" class = "btn btn-sm btn-danger text-white"  data-toggle="modal" data-target="#item-delete-modal{{$item->id}}">delete</a>

                          @include('thinkdashboard.franchise.pages.items.itemDeleteModal',['code'=>$item->id])
                        <a type="submit" class = "btn btn-sm btn-warning text-white"  data-toggle="modal" data-target="#item-rejected-modal{{$item->id}}">Reject</a>
                          @include('thinkdashboard.franchise.pages.items.itemRejectedModal',['code'=>$item->id])</td>

                    </tr>
                    @endforeach
                 </tbody>
                </table>
              </div>
            </div>
           <nav aria-label="Page navigation example">
                  {{$item_data->render("pagination::bootstrap-4") }}
               </nav>
          </div>
        </div>
@endsection
