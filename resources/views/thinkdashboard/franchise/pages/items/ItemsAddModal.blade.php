 <!-- delete Modal -->
<div class="modal fade" id="modal-category-add">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <h4 class="modal-title">Add new catagory</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
           
            </div>
            <div class="modal-body">
            

            <form method="POST" action="{{route('inventory.category.create')}}">
               {{csrf_field()}}

            <!-- 1st row -->

                 <div class="row">
                  <div class="col-md-6">
                    <div>
                    <input class="form-control my-2 form-control-user" id="category_name" name="category_name" type="text" value="{{old('category_name') }}" placeholder="Enter Category name " required="">
                    <p style="color:red;">{{ $errors->first('category_name') }}</p>
                    </div>

                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                    <div>
                    <input class="form-control my-2 form-control-user" id="category_slug" name="category_slug" type="text" value="{{ old('category_slug') }}" placeholder="Enter Category Slug" required="">
                    <p style="color:red;">{{ $errors->first('category_slug') }}</p>
                    </div>

                  </div>
                  <!-- /.col -->

                 </div>
                <!-- /.row -->


                <div class="row">
                     <div class="col-md-12">
                            <textarea maxlength="150" class="form-control py-3 form-control-user" id="category_description" name="category_description" type="text" placeholder="Enter description here (100 characters)" required="">{{ old('category_description') }}</textarea>
                             <p style="color:red;">{{ $errors->first('category_description') }}</p>
                     </div>
                    
                     {{-- /.col --}}


                </div>
               
                <div>
                </div>

            </div>
            <div class="modal-footer">
            <a  href="#" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</a>
            <button type="submit" class="btn btn-primary">confirm</button>
            </div>
             </form>
         
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<!-- /. delete modal -->
