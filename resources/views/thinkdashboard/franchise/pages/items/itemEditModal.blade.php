 <!-- delete Modal -->
<div class="modal fade" id="modal-item-edit{{$code}}">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <h4 class="modal-title">Edit Item</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
           
            </div>
            <div class="modal-body">
            

            <form method="POST" action="{{route('inventory.items.update')}}">
               {{csrf_field()}}
               <input type="hidden" name="id" value="{{ $item->id }}">

            <!-- 1st row -->
        @php
              $category_data = App\Http\Controllers\InventoryController::categories();

        @endphp
                 <div class="row">
                <div class="col-md-6">
                  <select class="form-control my-2 form-control-user" name="category_name" required="">
                    <option readonly>---Select a category---</option>
                    @foreach($category_data as $cat_data)
                      @if($cat_data->category_name == $item->category_name)
                    <option selected value="{{$cat_data->category_name}}">{{ $cat_data->category_name }}</option>
                      @else
                    <option value="{{$cat_data->category_name}}">{{ $cat_data->category_name }}</option>
                      @endif
                    @endforeach
                </select>
              </div>
                <div class="col-md-6">
                <input  class="form-control my-2 form-control-user" type="text" name="item_name" placeholder="Item name" value="{{ $item->item_name }}" required="">
                </div>
               
            </div>
              <div class="row">
                 <div class="col-md-6">
                <input  class="form-control my-2 form-control-user" type="text" name="item_quantity" placeholder="Item quantity" value="{{ $item->number_of_items }}" required="">
              </div>

              <div class="col-md-6">
                <input  class="form-control my-2 form-control-user" type="text" name="item_amount" placeholder="Amount" value="{{ $item->amount }}" required="">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <input  class="form-control my-2 form-control-user" type="text" name="item_description" placeholder="Description" value="{{ $item->description }}" required="">
              </div>
            </div>
               
          
            </div>
            <div class="modal-footer">
            <a  href="#" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</a>
            <button type="submit" class="btn btn-primary">Update</button>
            </div>
             </form>
         
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<!-- /. delete modal -->
