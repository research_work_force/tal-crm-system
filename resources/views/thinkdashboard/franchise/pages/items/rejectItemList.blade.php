@extends('thinkdashboard.franchise.layouts.dashboard-layout')
@section('content')

 <!-- Begin Page Content -->
        <div class="container-fluid">


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-danger">Rejected Items</h6>
             
            
            </div>

 <div class="card-body">

              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  <tr>
                    <th>Item</th>
                    <th>Category</th>
                    <th>Number of Items</th>
                    <th>Price (&#8377) (per item)</th>
                    <th>Remarks</th>
                  </tr>
                  </thead>

                  <tbody>
                  @foreach($item_data as $item)
                  <tr>
                      <td>{{$item->item_name}}</td>
                      <td>{{$item->category_name}}</td>
                      <td>{{$item->no_of_items}}</td>
                      <td>&#8377 {{$item->amount}}</td>
                      <td>{{$item->remarks}}</td>
                  </tr>
                    @endforeach
                 </tbody>
                </table>
              </div>
            </div>
           <nav aria-label="Page navigation example">
                  {{$item_data->render("pagination::bootstrap-4") }}
               </nav>
          </div>
        </div>
@endsection
