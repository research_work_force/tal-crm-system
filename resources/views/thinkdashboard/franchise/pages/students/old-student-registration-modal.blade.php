 <!-- delete Modal -->
<div class="modal fade" id="modal-old-student-reg{{$coursecode}}">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <h4 class="modal-title">Old Registration</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
           
            </div>
            <div class="modal-body">
            

            <form method="POST" action="{{route('old.student.course.enroll')}}">
               {{csrf_field()}}

               <input type="hidden" name="coursecode" value="{{$coursecode}}">


            <!-- 1st row -->
@php 
     $student_data = App\Http\Controllers\StudentAuthController::studentListsForFranchise();
     $coupon_data = App\Http\Controllers\CouponController::couponListingByFranchise($coursecode);

@endphp
                 <div class="row">
                  <div class="col-md-12">
                    <div>
                    <input list="student" class="typeahead form-control my-2 form-control-user" id="student_code" name="student_code" type="text" value="{{ old('student_code') }}" placeholder="Write student name here">
                    <datalist id="student">
                    @foreach ($student_data as $student)
                    <option value="{{ $student->student_code }}">{{ $student->student_name }}</option>
                    @endforeach
                  </datalist>

                    <p style="color:red;">{{ $errors->first('student_code') }}</p>
                  
</div>
                  </div>
                  <!-- /.col -->
             
                 </div>
                <!-- /.row -->

                <div class="row">
                  <div class="col-md-12">
                    <div>
                    <input list="coupon" class="typeahead form-control my-2 form-control-user" id="coupon_code" name="coupon_code" type="text" value="{{ old('coupon_code') }}" placeholder="Apply Coupon Code">
                    <datalist id="coupon">
                    @foreach ($coupon_data as $cd)
                
                    <option value="{{ $cd->coupon_code }}">
            
                    @endforeach
                  </datalist>

                    <p style="color:red;">{{ $errors->first('coupon_code') }}</p>
                  
</div>
                  </div>
                  <!-- /.col -->
             
                 </div>
                <!-- /.row -->

                <div>
                </div>

            </div>
            <div class="modal-footer">
            <a  href="#" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</a>
            <button type="submit" class="btn btn-primary">Next</button>
            </div>
             </form>
         
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<!-- /. delete modal -->
