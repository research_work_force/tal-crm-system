@extends('thinkdashboard.franchise.layouts.dashboard-layout')
@section('content')


        <!-- Begin Page Content -->
        <div class="container-fluid">


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Students</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  <tr>
                    <th>Image</th>
                    <th>Student Code</th>
                    <th>Email</th>
                    <th>Student Name</th>
                    <th>Contact</th>
                    <th>Address</th>
                    <th>Gardian Name</th>
                    <th>Gardian Number</th>
                    <th>Gardian Email</th>
                    <th>Institue</th>
                    <th>Future Goal</th>
                    <th>Adhaar Doc(Link)</th>
                    <th>Other Docs</th>
                    <th>Status</th>

                    <th>Action</th>
                  </tr>
                  </thead>

                  <tbody>
                  @foreach($student_data as $user)
                  <tr>
                     <td><img src="{{asset('upload/student/photo')}}/{{$user->photo}}" height="50" width="50"></>
                      <td>{{$user->student_code}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->student_name}}</td>
                      <td>{{$user->contact}}</td>
                      <td>{{$user->address}}</td>
                      <td>{{$user->g_name}}</td>
                      <td>{{$user->g_contact}}</td>
                      <td>{{$user->g_email}}</td>
                      <td>{{$user->institute_name}}</td>
                      <td>{{$user->future_goal}}</td>
                      <td>
                      
                        <a class="btn btn-sm btn-info" href="{{ asset('upload/student/adhaar_card') }}/{{$user->adhaar_card}}" target="_blank">View</a></td>

                        <td>
                       
                        @if ($user->other_docs != 'default.png')
                        <a class="btn btn-sm btn-info" href="{{ asset('upload/student/other_docs') }}/{{$user->other_docs}}" target="_blank">View</a></td>
                        @else
                           No Documents
                        @endif


                        <td>
                             
                        </td>

                      <td>
                            <!-- Edit delete -->
                         <div class="d-flex flex-row">
                               <div class="edit">
                                <a class = "btn btn-sm btn-info text-white mr-2" type="submit" data-toggle="modal" data-target="#modal-edit{{$user->id}}">edit</a>
                               </div>
                            @include('thinkdashboard.franchise.pages.students.student-edit-modal',['stdt_code'=>$user->student_code,'id'=>$user->id])
                         </div>
                    </td>
                    </tr>
                    @endforeach
                 </tbody>
                </table>
              </div>
            </div>
           <nav aria-label="Page navigation example">
                {{ $student_data->render("pagination::bootstrap-4") }}
               </nav>
          </div>
        </div>
@endsection
