<!-- edit modal -->

<div class="modal fade" id="modal-edit{{$id}}" tabindex="-1" role="dialog" >
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit Student Data</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            
            </div>
        <div class="modal-body">
@php
     $stdt_data = App\Http\Controllers\StudentAuthController::studentListByStudentCode($stdt_code); 

@endphp
<form method="POST" action="{{ route('student.info.update') }}" enctype="multipart/form-data">
    {{csrf_field()}}
   <input type="hidden" name="stdt_code" value="{{ $stdt_code }}">
   
    <div class="form-group has-feedback">
      <p class="mt-2"><b>Email</b></p>
    <input id="email" name="email"  value= "{{$stdt_data->email}}"  type="text" class="form-control" >
    <p style="color:red;">{{ $errors->first('email') }}</p>
    </div>

    <div class="form-group has-feedback">
      <p class="mt-2"><b>Student Name</b></p>
    <input id="student_name" name="student_name"  value= "{{$stdt_data->student_name}}"  type="text" class="form-control">
    <p style="color:red;">{{ $errors->first('student_name') }}</p>
    </div>

    <div class="form-group has-feedback">
      <p class="mt-2"><b>Contact</b></p>
    <input id="contact" name="contact" type="text" value= "{{$stdt_data->contact}}"  class="form-control">
    <p style="color:red;">{{ $errors->first('contact') }}</p>
    </div>

    <div class="form-group has-feedback">
      <p class="mt-2"><b>Address</b></p>
    <input id="address" name="address"  value= "{{$stdt_data->address}}" type="text" class="form-control">
    <p style="color:red;">{{ $errors->first('address') }}</p>
    </div>

    <div class="form-group has-feedback">
      <p class="mt-2"><b>Guardian Name</b></p>
    <input id="g_name" name="g_name"  value= "{{$stdt_data->g_name}}" type="text" class="form-control">
    <p style="color:red;">{{ $errors->first('g_name') }}</p>
    </div>

    <div class="form-group has-feedback">
      <p class="mt-2"><b>Guardian Contact</b></p>
    <input id="g_contact" name="g_contact"  value= "{{$stdt_data->g_contact}}" type="text" class="form-control">
    <p style="color:red;">{{ $errors->first('g_contact') }}</p>
    </div>

    <div class="form-group has-feedback">
      <p class="mt-2"><b>Guardian Email</b></p>
    <input id="g_email" name="g_email"  value= "{{$stdt_data->g_email}}" type="text" class="form-control">
    <p style="color:red;">{{ $errors->first('g_email') }}</p>
    </div>

    <div class="form-group has-feedback">
      <p class="mt-2"><b>Institue Name</b></p>
    <input id="institute_name" name="institute_name"  value= "{{$stdt_data->institute_name}}" type="text" class="form-control" >
    <p style="color:red;">{{ $errors->first('institute_name') }}</p>
    </div>

    <div class="form-group">
            <p class="mt-2"><b>Future Goal</b></p>
            <select class="form-control" id="sel1" name="future_goal" >
              @if($stdt_data->future_goal == "Entreprenuer")
              <option selected >{{$stdt_data->future_goal}}</option>
              <option value="Innovator">Innovator</option>
              <option value="Freelancer">Freelancer</option>
              <option value="Other">Other</option>
              @elseif($stdt_data->future_goal == "Innovator")
              <option selected >{{$stdt_data->future_goal}}</option>
              <option value="Entreprenuer">Entreprenuer</option>
              <option value="Freelancer">Freelancer</option>
              <option value="Other">Other</option>
              @elseif($stdt_data->future_goal == "Freelancer")
              <option selected >{{$stdt_data->future_goal}}</option>
              <option value="Entreprenuer">Entreprenuer</option>
              <option value="Innovator">Innovator</option>
              <option value="Other">Other</option>
              @elseif($stdt_data->future_goal == "Other")
              <option selected >{{$stdt_data->future_goal}}</option>
              <option value="Entreprenuer">Entreprenuer</option>
              <option value="Innovator">Innovator</option>
              <option value="Freelancer">Freelancer</option>
              @else
              <option value="Entreprenuer">Entreprenuer</option>
              <option value="Innovator">Innovator</option>
              <option value="Freelancer">Freelancer</option>
              <option value="Other">Other</option>
              @endif

            </select>
            <p style="color:red;">{{ $errors->first('future_goal') }}</p>
          </div>

        <!-- /.modal-body -->

        <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </div>

    </form>
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

</div>
<!-- /.modal -->


<!-- /.edit modal -->


