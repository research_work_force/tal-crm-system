@extends('thinkdashboard.franchise.layouts.dashboard-layout')
@section('content')


<div class="container">

<div class="card o-hidden border-0 shadow-lg my-5">
  <div class="card-body p-0">
    <!-- Nested Row within Card Body -->
    <div class="row">
      <!-- <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
      <div class=" col-lg-12">
        <div class="container">
        <div class="p-5">
          <div class="text-center">
            <h1 class="h4 text-gray-900 mb-4">Course Registration</h1>
          </div>

<form  method="POST" action="{{route('student.course.submit')}}" enctype="multipart/form-data">
    {{csrf_field()}}
<input type="hidden" name="course_code" value="{{ $course_data->course_code }}">
<Div>
    <!-- 1st row -->
 <div class="row">
  <div class="col-lg-5 col-md-4"><p class="mt-2"><b>Course details:</b><br></p>
  </div>
 </div>
    <div class="row">
         
          <!-- /.col -->

          <div class="col-md-4">
            <div class="form-group">
                
                <input class="form-control my-2 form-control-user" value="{{ $course_data->course_name }}" disabled>

                <p class="error-p" style="color:red;"></p>
              </div>

          </div><br><br>
          <!-- /.col -->
          <div class="col-md-4">
            <div class="form-group">
                
                <input class="form-control my-2 form-control-user" value="{{ $course_data->course_duration }} Hrs" disabled>
                <p class="error-p" style="color:red;"></p>
              </div>

          </div>
          <!-- /.col -->
           <div class="col-md-4">
            <div class="form-group">
                
                <input class="form-control my-2 form-control-user" value="{{ $course_data->course_fees }} /-" disabled>

                <p class="error-p" style="color:red;"></p>
              </div>

          </div>
          <!-- /.col -->

         </div>

    <!-- 2nd row -->
    <div class="row">
      <div class="col-md-6">
       <p class="mt-2"><b>Student details:</b></p>
     </div>
    </div>

    <!-- 2nd row -->

         <div class="row">
          <div class="col-md-6">

            <div>
            <input class="form-control my-2 form-control-user" id="email" name="email" type="email" value="{{ old('email') }}" placeholder="Email-Id (If any otherwise put NA)"  required>
            <p class="error-p" style="color:red;">{{ $errors->first('email') }}</p>
            </div>

          </div>
          <!-- /.col -->


          <div class="col-md-6">
            <div>
            <input class="form-control my-2 form-control-user" id="student_name" name="student_name" type="text" value="{{ old('student_name') }}" placeholder="Student Name" required>
            <p class="error-p" style="color:red;">{{ $errors->first('student_name') }}</p>
            </div>


          </div>
          <!-- /.col -->

         </div>
        <!-- /.row -->
<!-- 3nd row -->
        <div class="row">
          <div class="col-md-5">
              

                <div>
                <input class="form-control my-2 form-control-user" name="contact" value="{{ old('contact') }}" type="number" placeholder="Students contact number" onKeyPress="if(this.value.length==10) return false;" required>
                <p class="error-p" style="color:red;">{{ $errors->first('contact') }}</p>
                </div>


          </div>
          <!-- /.col -->
          <div class="col-md-7">
          <div class="form-group">
           <textarea class="form-control my-2"  id="address" name="address" type="text" value="" placeholder="Enter Address here" rows="3" required>{{ old('address') }}</textarea>
           <p class="error-p" style="color:red;">{{ $errors->first('address') }}</p>
           </div>

          </div>
          <!-- /.col -->

         </div>
        <!-- /.row -->

    <!-- 4th row -->

    <div class="row">
          <div class="col-md-4">
                <div>
                        <input class="form-control my-2 form-control-user" id="g_name" name="g_name" type="text" value="{{ old('g_name') }}" placeholder="Guardian's name" required>
                        <p class="error-p" style="color:red;">{{ $errors->first('g_name') }}</p>
                        </div>
          </div>
          {{-- .col --}}


          <div class="col-md-4">
                <div>
                        <input class="form-control my-2 form-control-user" name="g_contact" type="number" value="{{ old('g_contact') }}" placeholder="Guardian's contact number" onKeyPress="if(this.value.length==10) return false;" required>
                        <p class="error-p" style="color:red;">{{ $errors->first('g_contact') }}</p>
                        </div>
          </div>
          <!-- /.col -->
          <div class="col-md-4">
                <div>
                    <input class="form-control my-2 form-control-user" id="email" name="g_email" type="text" value="{{ old('g_email') }}" placeholder="Guardian's email here (If any otherwise put NA)" required>
                    <p class="error-p" style="color:red;">{{ $errors->first('g_email') }}</p>
                </div>


              </div>
              <!-- /.col -->


         </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-9">
                    <div class="form-group">
                            <input class="form-control my-2 form-control-user" id="institue_name" name="institue_name" type="text" value="{{ old('institue_name') }}" placeholder="Institue Name" required>
                            <p class="error-p" style="color:red;">{{ $errors->first('institue_name') }}</p>
                            </div>

            </div>

           <div class="col-md-3">
            <div class="form-group">
          
                <select class="form-control my-2 form-control-user" id="sel1" name="future_goal" required>
                  <option readonly>Future Goal</option>
                  <option value="Entreprenuer">Entreprenuer</option>
                  <option value="Innovator">Innovator</option>
                  <option value="Freelancer">Freelancer</option>
                  <option value="Other">Other</option>

                </select>
                <p class="error-p" style="color:red;">{{ $errors->first('future_goal') }}</p>
              </div>

          </div>
          <!-- /.col -->

        </div>

   </div>

         
        <!-- /.row -->

        <!-- Upload PAN, Adhar or Trade Licence -->

        <div class="row">
              <div class="col-md-4">
                 <p class="mt-2"><b>Student Documents:</b></p>
               </div>
             </div>
                 
           <div class="row mb-3">
              <div class="col-md-4">
                 <p class="mt-2"><b>Adhaar card:</b></p>

                    <!-- Custom bootstrap upload file-->
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" value="{{ old('adhaar_card') }}" id="customFile"  name="adhaar_card" required>
                      <p class="error-p" style="color:red;">{{ $errors->first('adhaar_card') }}</p>

                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>

          <!-- End -->

              </div>
              {{-- /.col --}}

                <div class="col-md-4">
                <p class="mt-2"><b>Photo:</b></p>
                <!-- Custom bootstrap upload file-->
                <div class="custom-file">
                        <input type="file" class="custom-file-input" value="{{ old('photo') }}" id="customFile"  name="photo" required>
                        <p class="error-p" style="color:red;">{{ $errors->first('photo') }}</p>

                        <label class="custom-file-label" for="customFile">Choose file</label>
                </div>

             </div>

             
             {{-- /.col --}}
          <div class="col-md-4">
                <p class="mt-2"><b>Other Support Documents:</b></p>
                <!-- Custom bootstrap upload file-->
                <div class="custom-file">
                        <input type="file" class="custom-file-input" value="{{ old('other_docs') }}" id="customFile"  name="other_docs">
                        <p class="error-p" style="color:red;">{{ $errors->first('other_docs') }}</p>

                        <label class="custom-file-label" for="customFile">Choose file</label>
                </div>



         <!-- End -->

             </div>
             {{-- /.col --}}

           </div>

           <div class="row">

              <div class="col-md-4">
                 <p class="mt-2"><b>Coupon apply:</b></p>
               </div>
            </div>
            <div class="row">

               <div class="col-md-3">
            <div class="form-group">
                         <select class="form-control"  name="coupon_code_applied" required>
                           <option disabled>Select a Coupon</option>
                          
                                @foreach ($coupon_data as $cd)
                                   
                                       <option value="{{$cd->coupon_code}}">{{$cd->coupon_code}}</option>
                                   
                                @endforeach
                           
                          </select>
                          <p style="color:red;">{{ $errors->first('coupon_code_applied') }}</p>

                                    </div>

          </div>

           </div>

        

        <div>
        <button type="submit" class="btn btn-primary ">Next</button>
        </div>

</form>
<!-- register form end -->
</div>
</div>
      </div>
    </div>
  </div>
</div>

</div>

<script>

</script>

@endsection
