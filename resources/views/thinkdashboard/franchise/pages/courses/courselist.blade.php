@extends('thinkdashboard.franchise.layouts.dashboard-layout')
@section('content')

<div class="container">
    <header class="bold headingOne">Offered Courses</header>

    <div class="row">
        @foreach ($course as $item)

        @if($item->status == 1)
        <div class="col-md-4 my-3" style="display: ;">
                <div class="card card-shadow">
                    <div class="zoom-img-holder">
                        <img  class="card-img-top img-dark-zoom" src="{{asset('upload/course/thumbnail/')}}/{{$item->thumbnail}}" alt="">
                    </div>
                    <div class="card-body">
                        <p class="bolder color-text">{{ $item->course_name }}({{ $item->course_code }})</p>
                         <p class="small-p course_description">{{ str_limit($item->course_description, 155) }}</p>
                        <div class="clearfix">
                            <div class="float-left">
                                <p class="small-p-bold"><i class="fas fa-rupee-sign"></i>{{ $item->course_fees }}</p>

                            </div>
                            <div class="float-right">
                                <p class="small-p-bold">{{ $item->course_duration }} Hours</p>

                            </div>

                        </div>
                        {{-- /.clearfix --}}

                        <div class="clearfix">
                            <div class="row">
                                <div class="col-md-12">
                                <div class="float-left">

                                     
                                
                                <a href="{{ route('student.course.create',[$item->course_code])}}" class = "btn btn-sm btn-success text-white mr-2" >New Registration</a>

                                <a class = "btn btn-sm btn-warning text-white mr-2"  data-toggle="modal" data-target="#modal-old-student-reg{{$item->course_code}}" href="">Old Registration</a></h6>
                        @include('thinkdashboard.franchise.pages.students.old-student-registration-modal',['coursecode'=>$item->course_code])

                   
                                </div>
                            </div>
                        </div>
                         <div class="row">
                                <div class="col-md-12">
                                <div class="float-right">

                                    @php
                                        $tags_array = explode(",", $item->course_tags);
                                    @endphp


                                    @foreach ($tags_array as $tags)

                                    <span href="#" class="btn  m-0 p-0"><span class="badge badge-secondary">{{ $tags }}</span></span>

                                    @endforeach

                                </div>
                            </div>
                        </div>

                        </div>
                        {{-- /.clearfix --}}


                    </div>
                </div>
            </div>
            @endif

        @endforeach
   

</div>
 {{ $course->render("pagination::bootstrap-4") }}
@endsection

