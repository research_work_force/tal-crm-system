@extends('thinkdashboard.franchise.layouts.dashboard-layout')
@section('content')

<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
         <div class="row">
            <div class="col-md-12">
            <div class="container">
            <div class="p-5">
                <div class="text-center">
                    <h4>Add Category</h4>
                </div>
                <div class="clearfix sticky-top">
                    <div class="float-right">
                            <div class="bulk ">
                                   
                                    <a class = "btn btn-info text-white" href="{{route('inventory.category.list')}}">Show list</a>                                  

                            </div>
                    </div>
                </div>

              <form method="POST" action="{{route('inventory.category.create')}}">
               {{csrf_field()}}

            <!-- 1st row -->

                 <div class="row">
                  <div class="col-md-6">
                    <div>
                    <input class="form-control my-2 form-control-user" id="category_name" name="category_name" type="text" value="{{ old('category_name') }}" placeholder="Enter Category name " required="">
                    <p style="color:red;">{{ $errors->first('category_name') }}</p>
                    </div>

                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                    <div>
                    <input class="form-control my-2 form-control-user" id="category_slug" name="category_slug" type="text" value="{{ old('category_slug') }}" placeholder="Enter Category Slug" required="">
                    <p style="color:red;">{{ $errors->first('category_slug') }}</p>
                    </div>

                  </div>
                  <!-- /.col -->

                 </div>
                <!-- /.row -->


                <div class="row">
                     <div class="col-md-6">
                            <textarea maxlength="150" class="form-control py-3 form-control-user" id="category_description" name="category_description" type="text" placeholder="Enter description here (100 characters)" required="">{{ old('category_description') }}</textarea>
                             <p style="color:red;">{{ $errors->first('category_description') }}</p>
                     </div>
                    
                     {{-- /.col --}}


                </div>
               
                <div>
                <button type="submit" class="btn btn-primary ">Submit</button>
                </div>

          </form>
        </div>
        </div>
          </div>
        </div>
      </div>
    </div>
</div>



@endsection
