@extends('thinkdashboard.franchise.layouts.dashboard-layout')
@section('content')

 <!-- Begin Page Content -->
        <div class="container-fluid">


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Categories <a class = "btn btn-sm btn-primary "  data-toggle="modal" data-target="#modal-category-add" href=""><i class="fa fa-plus-circle" aria-hidden="true"></i></a></h6>
              @include('thinkdashboard.franchise.pages.category.category-add-modal')
            
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  <tr>
                    <th>Category Name</th>
                    <th>Slug</th>
                    <th>Description</th>
                    <th>Action</th>
                  </tr>
                  </thead>

                  <tbody>
                  @foreach($item_category_data as $item)
                  <tr>
                      <td>{{$item->category_name}}</td>
                      <td>{{$item->slug}}</td>
                      <td>{{$item->description}}</td>
                      <td> 
                        <a type="submit" class = "btn btn-sm btn-danger text-white"  data-toggle="modal" data-target="#modal-delete{{$item->id}}">delete</a>
                          @include('thinkdashboard.franchise.include.modal.category-delete-modal',['code'=>$item->id])</td>
                    </tr>
                    @endforeach
                 </tbody>
                </table>
              </div>
            </div>
           <nav aria-label="Page navigation example">
                  {{$item_category_data->render("pagination::bootstrap-4") }}
               </nav>
          </div>
        </div>
@endsection
