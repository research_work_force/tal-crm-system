 <!-- delete Modal -->
<div class="modal fade" id="modal-expenses-add">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <h4 class="modal-title">Expenses</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
           
            </div>
            <div class="modal-body">
            

            <form method="post" action="{{route('franchise.expenses.create')}}">
               {{csrf_field()}}

            <!-- 1st row -->

                 <div class="row">
                  <div class="col-md-6">
                    
                    <input class="form-control my-2 form-control-user" id="date" name="date" type="date" value="{{ old('date') }}" placeholder="Date">
                    <p style="color:red;">{{ $errors->first('date') }}</p>
                    

                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                    <div>
                    <input class="form-control my-2 form-control-user" id="amount" name="amount" type="text" value="{{ old('amount') }}" placeholder="Amount">
                    <p style="color:red;">{{ $errors->first('amount') }}</p>
                    </div>

                  </div>
                  <!-- /.col -->

                 </div>
                <!-- /.row -->


                <div class="row">
                     <div class="col-md-12">
                             <textarea class="form-control my-2 form-control-user" id="statement" name="statement" type="text" value="{{old('statement') }}" placeholder="Statement"></textarea>
                    <p style="color:red;">{{ $errors->first('statement') }}</p>

                     </div>
                    
                     {{-- /.col --}}


                </div>
               
                <div>
                </div>

            </div>
            <div class="modal-footer">
            <a  href="#" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</a>
            <button type="submit" class="btn btn-primary">confirm</button>
            </div>
             </form>
         
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<!-- /. delete modal -->
