 <!-- delete Modal -->
<div class="modal fade" id="modal-registration-due{{$id}}">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <h4 class="modal-title">Expenses</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
           
            </div>
            <div class="modal-body">
            

            <form method="post" action="{{route('accounts.dues.pay')}}">
               {{csrf_field()}}

            <!-- 1st row -->

                 <div class="row">
                  <div class="col-md-6">
                    
                    <input class="form-control my-2 form-control-user" id="student_code" name="student_code" type="text" value="{{ $regdata->student_code }}" placeholder="Student Code" readonly>
                    

                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                    <div>
                    <input class="form-control my-2 form-control-user" id="payment_id" name="payment_id" type="text" value="{{ $regdata->payment_id }}" placeholder="Payment ID" readonly>
                    </div>

                  </div>
                  <!-- /.col -->

                 </div>
                <!-- /.row -->


                <div class="row">
                     <div class="col-md-12">
                    
                    <input class="form-control my-2 form-control-user" id="amount" name="amount" value="{{ $regdata->amount_due }}" type="text"  placeholder="Amount">
                    

                     </div>
                    
                     {{-- /.col --}}


                </div>
               
                <div>
                </div>

            </div>
            <div class="modal-footer">
            <a  href="#" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</a>
            <button type="submit" class="btn btn-primary">Pay</button>
            </div>
             </form>
         
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<!-- /. delete modal -->
