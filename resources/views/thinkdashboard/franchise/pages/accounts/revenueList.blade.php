@extends('thinkdashboard.franchise.layouts.dashboard-layout')
@section('content')


        <!-- Begin Page Content -->
        <div class="container-fluid">
           <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Filter revenue by dates</h6>
            </div>
            <div class="card-body">
                <form  method="get" action="{{route('revenue.list.rangedate')}}" >
                  {{csrf_field()}}
              <div>
                  <!-- 1st row -->
               <div class="row">
                <div class="col-lg-5 col-md-4"> <p class="mt-2"><b></b></p>
                </div>
               </div>
                  <div class="row">
                       
                        <!-- /.col -->

                        <div class="col-md-4">
                          <div class="form-group">
                              
                              <input class="form-control my-2 form-control-user" type="date" name="fdate">

                              <p class="error-p" style="color:red;"></p>
                            </div>

                        </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                              <div class="form-group">
                                  
                                  <input class="form-control my-2 form-control-user" type="date" name="tdate">
                                  <p class="error-p" style="color:red;"></p>
                                </div>

                            </div>
                            <!-- /.col -->
                             <div class="col-md-4">
                              <div class="form-group">
                                  
                                  <button class="form-control my-2 btn btn-sm btn-info" type="submit">Go</button>
                                </div>

                            </div>
                            <!-- /.col -->

                           </div>

                         </div>
                       </form>
       <!-- Form end -->
            </div>
          </div>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Transactions
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  <tr>
                    <th>Date</th>
                    <th>Student Code</th>
                    <th>Course Name</th>
                    <th>Course Duration</th>
                    <th>Course Fees</th>
                    <th>Payment Id</th>
                    <th>Applied Coupon</th>
                    <th>Amount</th>
                    <th>Paid</th>
                    <th>Due</th>
                    
                  </tr>
                  </thead>

                  <tbody>
                 @foreach ($course_transaction_data as $data)
                  <tr>
                      <td>{{$data->date}}</td>
                      <td>{{$data->student_code}}</td>
                      <td>{{$data->course_name}}</td>
                      <td>{{$data->course_duration}}</td>
                      <td>{{$data->course_fees}}</td>
                      <td>{{$data->payment_id}}</td>
                      <td>{{$data->coupon_code}}</td>
                      <td>{{intVal($data->amount_paid) + intVal($data->amount_due) }}</td>
                      <td>{{$data->amount_paid}}</td>
                      <td>{{$data->amount_due}}</td>

                    </tr>
                  @endforeach
                 </tbody>
                </table>
              </div>
            </div>
           <nav aria-label="Page navigation example">
                 {{$course_transaction_data->render("pagination::bootstrap-4") }}
               </nav>
          </div>
        </div>
@endsection
