 <!-- delete Modal -->
<div class="modal fade" id="modal-expenses-edit{{$id}}">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <h4 class="modal-title">Edit</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
           
            </div>
            <div class="modal-body">
            

            <form method="post" action="{{route('franchise.expenses.edit')}}">
               {{csrf_field()}}
               <input type="hidden" name="id" value="{{ $id }}">
            <!-- 1st row -->

                 <div class="row">
                  <div class="col-md-6">
                    
                    <input class="form-control my-2 form-control-user" id="date" name="date" type="date" placeholder="Date" value="{{ $data->date }}">
                    <p style="color:red;">{{ $errors->first('date') }}</p>
                    

                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                    <div>
                    <input class="form-control my-2 form-control-user" id="amount" name="amount" type="text" value="{{ $data->amount }}" placeholder="Amount">
                    <p style="color:red;">{{ $errors->first('amount') }}</p>
                    </div>

                  </div>
                  <!-- /.col -->

                 </div>
                <!-- /.row -->


                <div class="row">
                     <div class="col-md-12">
                             <textarea class="form-control my-2 form-control-user" id="statement" name="statement" type="text" placeholder="Statement">{{ $data->statement }}</textarea>
                    <p style="color:red;">{{ $errors->first('statement') }}</p>

                     </div>
                    
                     {{-- /.col --}}


                </div>
               
                <div>
                </div>

            </div>
            <div class="modal-footer">
            <a  href="#" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</a>
            <button type="submit" class="btn btn-primary">confirm</button>
            </div>
             </form>
         
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<!-- /. delete modal -->
