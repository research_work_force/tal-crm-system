 <!-- delete Modal -->
<div class="modal fade" id="modal-emp-edit{{$id}}">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <h4 class="modal-title">Edit </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
           
            </div>
            <div class="modal-body">
            

            <form method="post" action="{{route('franchise.employee.edit')}}" enctype="multipart/form-data">
               {{csrf_field()}}

               <input type="hidden" name="id" value="{{$data->id}}">
               <div class="row">
                  <div class="col-md-12">
                    
                    <input class="form-control my-2 form-control-user" id="name" name="name" value="{{$data->name}}"placeholder="Name" required>
                    <p style="color:red;">{{ $errors->first('address') }}</p>
                    
                    
                    </div>

                  </div>
                  <!-- /.col -->

                   <div class="row">
                          <div class="col-md-12">
                            
                            <textarea class="form-control my-2 form-control-user" id="date" name="address" placeholder="address" required>{{$data->address}}</textarea>
                            <p style="color:red;">{{ $errors->first('address') }}</p>
                            
                            
                            </div>

                          </div>
                  <!-- /.col -->

                  <div class="row">
                  <div class="col-md-5">
                    
                    <input class="form-control my-2 form-control-user" id="date" name="contact_num" type="number" placeholder="Contact number" value="{{$data->contact_number}}" required>
               
                    
                    </div>

                    <div class="col-md-7">
                    
                    <input class="form-control my-2 form-control-user" id="identity_proof" name="identity_proof" type="file"  placeholder="Identity Proof">
                   

                    </div>
                  </div>

                  <div class="row">
                   <div class="col-md-5">
                   </div>
                   <div class="col-md-6">
                     
                     <img src="{{ asset('upload/empdoc/') }}/{{$data->identity_card}}" height="50" width="100">
                   </div>
                  </div>

            </div>
            <div class="modal-footer">
            <a  href="#" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</a>
            <button type="submit" class="btn btn-primary">confirm</button>
            </div>
             </form>
         
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<!-- /. delete modal -->





















