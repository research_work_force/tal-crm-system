@extends('thinkdashboard.franchise.layouts.dashboard-layout')
@section('content')


        <!-- Begin Page Content -->
        <div class="container-fluid">
           <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Filter employee by dates</h6>
            </div>
            <div class="card-body">
                <form  method="post" action="{{route('expenses.list.rangedate')}}" >
                  {{csrf_field()}}
              <div>
                  <!-- 1st row -->
               <div class="row">
                <div class="col-lg-5 col-md-4"> <p class="mt-2"><b></b></p>
                </div>
               </div>
                  <div class="row">
                       
                        <!-- /.col -->

                        <div class="col-md-4">
                          <div class="form-group">
                              
                              <input class="form-control my-2 form-control-user" type="date" name="fdate">

                              <p class="error-p" style="color:red;"></p>
                            </div>

                        </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                              <div class="form-group">
                                  
                                  <input class="form-control my-2 form-control-user" type="date" name="tdate">
                                  <p class="error-p" style="color:red;"></p>
                                </div>

                            </div>
                            <!-- /.col -->
                             <div class="col-md-4">
                              <div class="form-group">
                                  
                                  <button class="form-control my-2 btn btn-sm btn-info" type="submit">Go</button>
                                </div>

                            </div>
                            <!-- /.col -->

                           </div>

                         </div>
                       </form>
       <!-- Form end -->
            </div>
          </div>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Employees <a class = "btn btn-sm btn-primary "  data-toggle="modal" data-target="#modal-emp-add" href=""><i class="fa fa-plus-circle" aria-hidden="true"></i></a> </h6>
              
              @include('thinkdashboard.franchise.pages.employee.add-employee-modal')
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Contact Number</th>
                    <th>Address</th>
                    <th>Identity Doc</th>
                    <th>Action</th>
                  </tr>
                  </thead>

                  <tbody>
                 @foreach ($employee_data as $data)
                  <tr>
                      <td>{{$data->name}}</td>
                      <td>{{$data->contact_number}}</td>
                      <td>{{$data->address}}</td>
                      <td><img src="{{ asset('upload/empdoc/') }}/{{$data->identity_card}}" height="50" width="90"></td>
                      <td>
                            <!-- Edit delete -->
                         <div class="d-flex flex-row">
                               <div class="edit">
                                <a class = "btn btn-sm btn-info text-white mr-2" type="submit" data-toggle="modal" data-target="#modal-emp-edit{{$data->id}}">edit</a>
                               </div>
                             @include('thinkdashboard.franchise.pages.employee.edit-employee-modal',['id'=>$data->id,'data'=>$data])
                         </div>
                    </td>
                    </tr>
                  @endforeach
                 </tbody>
                </table>
              </div>
            </div>
           <nav aria-label="Page navigation example">
                 {{$employee_data->render("pagination::bootstrap-4") }}
               </nav>
          </div>
        </div>
@endsection
