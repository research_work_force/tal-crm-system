
@extends('thinkdashboard.franchise.layouts.dashboard-layout')
@section('content')

<div class="container">

  <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
         <!-- Nested Row within Card Body -->
         <div class="row">

            <div class=" col-lg-12">
              <div class="container">
                <div class="p-5">
                      <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">Payment Transaction</h1>
                      </div>

                  <form method="POST" action="{{route('student.course.payment')}}">
                    {{csrf_field()}}
                    <input type="hidden" name="student_code" value="{{$student_code}}">
                    <input type="hidden" name="coupon_code" value="{{$coupon_data->coupon_code}}">
                    <input type="hidden" name="course_code" value="{{$course_data->course_code}}">

                   <!-- 1st row -->

                     <div class="row">
                       <div class="col-md-3">
                         <div class="form-group">
                          <p class="mt-2"><b>Amount (to be paid):</b></p>
                                            <input class="form-control form-control-user" value="{{ $amount_need_to_be_paid }}" placeholder="percentage" disabled>

                                    </div>

                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                         <div class="form-group">
                          <p class="mt-2"><b>Coupon Code Applied:</b></p>
                                            <input class="form-control form-control-user" value="{{ $coupon_data->coupon_code }}"  disabled>

                                    </div>

                      </div>

                      <div class="col-md-3">
                         <div class="form-group">
                          <p class="mt-2"><b>Course Name:</b></p>
                                            <input class="form-control form-control-user" value="{{ $course_data->course_name }}"  disabled>

                                    </div>

                      </div>

                     </div>
                    <!-- /.row -->
                      <div class="row">
                        <div class="col-md-5">
                           <div class="form-group">
                           <p class="mt-2"><b>Amount Paid:</b></p>
                          <input class="form-control my-2 form-control-user" id="amt_paid" name="amt_paid" value="{{ old('contact') }}" type="number" placeholder="Amount" required onchange="pricecal();">
                        </div>
                        </div>
                        
                          <div class="col-md-5">
                           <div class="form-group">
                           <p class="mt-2"><b>Amount Due:</b></p>
                          <input class="form-control my-2 form-control-user" id="amt_due" name="amt_due" value="{{ old('contact') }}" type="number" placeholder="Due amount" required readonly>
                          </div>
                          </div>

<script type="text/javascript">
  function pricecal()
  {
     var amt = <?php echo $amount_need_to_be_paid; ?> ;
     var amt_paid = document.getElementById('amt_paid').value;

     var amt_due = amt - amt_paid;

     document.getElementById('amt_due').value = amt_due;

  }

</script>
                      </div>
                    <!-- 2nd row -->
                    <div class="row">
                        <div class="col-md-3">
                                 <p class="mt-2"><b>Select Mode of Payment</b></p>

                        </div>
                        <!-- /.col -->
                      </div>

                      <div class="row">
                        <div class="col-md-5">
                                    <div class="form-group">
                                               
                                           <select class="form-control"  id="mode_of_payment" name="mode_of_payment" onchange="modeOfPayment();">
                                           <option selected disabled>Payment Type</option>
                                                @foreach ($payment_type as $pt)
                                                  <option value="{{$pt->type_code}}">{{$pt->type_name}}</option>
                                                @endforeach
                                          </select>
                                          <p style="color:red;">{{ $errors->first('mode_of_payment') }}</p>

                                    </div>
                       </div>
<script type="text/javascript">
  function modeOfPayment()
  {
     var type_code = document.getElementById('mode_of_payment').value;

     if (type_code==1){
          document.getElementById('denominators').style.display = "block";
          document.getElementById('denominators').required = true; 
          document.getElementById('transaction_id').required = false; 
          document.getElementById('transaction_id').style.display = "none";
     }
     else if(type_code==2) {
          document.getElementById('transaction_id').style.display = "block";
          document.getElementById('transaction_id').required = true; 
          document.getElementById('denominators').required = false; 
          document.getElementById('denominators').style.display = "none";
     }
     else {

          document.getElementById('transaction_id').style.display = "none";
          document.getElementById('denominators').style.display = "none";

     }
  }


</script>
                        <!-- /.col -->
                        <div class="col-md-4">
                                   <textarea maxlength="150" class="form-control py-3 form-control-user" id="denominators" name="denominators" placeholder="Write the Denominations" style="display: none;"></textarea>



                                   <input class="form-control form-control-user" id="transaction_id" name="cheque_number" placeholder="Cheque Number" style="display: none;">
                        </div>

                      <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-md-5">
                           <div class="form-group">
                           <p class="mt-2"><b>Statement/Description</b></p>
                          <textarea maxlength="100" class="form-control py-3 form-control-user" id="description" name="description" placeholder="Description (Limit 100 Characters)"></textarea>
                        </div>
                        </div>
                      </div>

                       <div class="mt-3">
                       <button type="submit" class="btn btn-primary ">Submit</button>
                       </div>

                  </form>
                 <!-- register form end -->
                </div>
                {{-- /.p-5 --}}
              </div>
              {{-- /.container --}}
            </div>
            {{-- /.col --}}
         </div>
         {{-- /.row --}}
      </div>
      {{-- /.card-body --}}
 </div>
 {{-- /.card --}}

</div>
{{-- /.conatiner --}}





@endsection
