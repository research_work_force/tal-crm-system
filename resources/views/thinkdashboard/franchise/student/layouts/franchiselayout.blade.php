<!DOCTYPE html>
<html lang="en">
<head>
      @include('thinkdashboard.franchise.include.headerlinks')
      <title>Admin</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">

     @include('thinkdashboard.franchise.include.navbar')

     @include('thinkdashboard.franchise.include.mainsidebar')

     @yield('content')
     
     @include('thinkdashboard.franchise.include.footer')
    
     </div>

     @include('thinkdashboard.franchise.include.footerlinks')
  
</body>
</html>