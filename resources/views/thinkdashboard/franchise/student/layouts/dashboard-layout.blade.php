<!DOCTYPE html>
<html lang="en">
<head>
      @include('thinkdashboard.franchise.student.include.headerlinks')
      <title>Student</title>
</head>
<body id="page-top">
   <div id="wrapper">


     @include('thinkdashboard.franchise.student.include.mainsidebar')

      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">


      <!-- Main Content -->
      <div id="content">

      @include('thinkdashboard.franchise.student.include.navbar')

      @yield('content')

            <!-- error handaler -->
        @if ($errors->any())
        <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>
        @endif

        <!-- flash massage show -->
        @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
        @endif




     </div>
     <!-- content end -->
     @include('thinkdashboard.franchise.student.include.footer')

     </div>
     <!-- main content end -->
     </div>

     @include('thinkdashboard.franchise.student.include.footerlinks')

</body>
</html>

