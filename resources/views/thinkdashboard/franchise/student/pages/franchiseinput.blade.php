@extends('thinkdashboard.franchise.layouts.franchiselayout')
@section('content')

<div class = "container-fluid content-wrapper " >
<div class="register-box box box-primary">
  <!-- <div class="register-logo">
    <a href="../../index2.html"><b>Admin</b>LTE</a>
  </div> -->

  <div class="register-box-body">
    <h3 class="login-box-msg">Register a new membership</h3>

    <form method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Full name">
        <!-- <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Address">
        <!-- <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Phone Number">
        <!-- <span class="glyphicon glyphicon-envelope form-control-feedback"></span> -->
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email">
        <!-- <span class="glyphicon glyphicon-envelope form-control-feedback"></span> -->
      </div>
      
      
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Location">
        <!-- <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
      </div>
     
      
      
        
        <div class="submit">
          <button type="submit" class="btn btn-primary">Register</button>
        </div>
        <!-- /.submit -->
    
    </form>
<!-- 
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a>
    </div> -->

    <a href="#" class="text-center">I already have a account</a>
  </div>
  <!-- /.register-box -->
</div>
<!-- /.register-box-body -->
</div>
<!-- /.container-fluid -->



@endsection