@extends('thinkdashboard.admin.layouts.login-layout')
@section('content')


<div class="container">

<!-- Outer Row -->
<div class="row justify-content-center">

  <div class="col-xl-10 col-lg-12 col-md-9">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
          <div class="col-lg-6">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Welcome Back Student!</h1>
              </div>
              <form class="user" method="POST" action="{{ route('student.login')}}">
            {{csrf_field()}}
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="u_name" id="exampleInputu_name"  placeholder="Enter Username Here...">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-user" name="password" id="exampleInputPassword" placeholder="Password">
                </div>
                <div class="form-group">
                  <div class="custom-control custom-checkbox small">
                    <input type="checkbox" class="custom-control-input" id="customCheck">
                    <label class="custom-control-label" for="customCheck">Remember Me</label>
                  </div>
                </div>
                <div>
                <button type="submit" class="btn btn-primary btn-user btn-block">Login</button>
                </div>

              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="forgot-password.html">Forgot Password?</a>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

</div>
  <!-- error handaler -->
  @if ($errors->any())
  <div class="alert alert-danger">
  <ul>
      @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
      @endforeach
  </ul>
  </div>
  @endif

  <!-- flash massage show -->
  @if(Session::has('message'))
  <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
  @endif
</div>



@endsection
