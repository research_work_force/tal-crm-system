<!-- Bulk Modal -->
<div class="modal fade" id="franchise-doc-edit"  tabindex="-1" role="dialog" >
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center">Franchise Upload New Documents</h4>
            </div>
             <div class="modal-body">
                <form method="POST" action="{{route('franchise.documents.upload',$user_name)}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="row">
                            <div class="col-md-12">
                                    <div class="custom-file">
                                    <input type="hidden" name="user_name" value="{{$user_name}}">

                                            <input type="file" name="documents[]" class="custom-file-input" id="customFile" multiple>
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                            </div>

                    </div>

           <div>
                <p class="error-p">{{ $errors->first('documents') }}</p>
           </div>

            </div>
            {{-- /.modal body --}}

                     <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                     </div>
                </form>

        </div>

        <!-- /.modal-dialog -->
</div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- /. delete modal -->
