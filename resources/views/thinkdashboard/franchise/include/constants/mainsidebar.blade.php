<!-- Sidebar -->
<ul class="navbar-nav sidebar-background-color sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="tal-brand">
  <div class="sidebar-brand-icon ">
    <img src="{{asset('thinkdashboard/assets/img/logo/tal-logo.png')}}" width="300px" alt="">
  </div>
  
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<?php 
function activeMenu($uri = '') {
    $active = '';
    if (Request::is(Request::segment(1) . '/' . $uri . '/*') || Request::is(Request::segment(1) . '/' . $uri) || Request::is($uri)) {
        $active = 'active';
    }
    return $active;
}
?>

<!-- Nav Item - Dashboard -->
<!-- <li class="nav-item active">
  <a class="nav-link" href="/admin/dashboard">
    <i class="fas fa-fw fa-tachometer-alt"></i>
    <span>Dashboard</span></a>
</li> -->

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
{{-- <div class="sidebar-heading">
  Interface
</div> --}}

<!-- Nav Item - Utilities Collapse Menu -->
<li class="nav-item {{ activeMenu('course') }}">
  <a class="nav-link" href="/franchise/course/list"  aria-controls="collapseUtilities">
    <i class="fas text-white fa-graduation-cap"></i>
    <span>Dashboard</span>
  </a>
 
</li>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item {{ activeMenu('student') }}">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
    <i class="fas fa-user-friends text-white"></i>
    <span>Students</span>
  </a>
  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      {{-- <h6 class="collapse-header"></h6> --}}

      <a class="collapse-item" href="{{route('student.list')}}">List</a>
    </div>
  </div>
</li>


<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item {{ activeMenu('inventory') }}">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#inventorycollapse" aria-expanded="true" aria-controls="inventorycollapse">
    
    <i class="fa fa-list text-white" aria-hidden="true"></i>
    <span>Inventory</span>
  </a>
  <div id="inventorycollapse" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      {{-- <h6 class="collapse-header"></h6> --}}

      <a class="collapse-item" href="{{route('inventory.category.view')}}">Add Category</a>
   
      {{-- <h6 class="collapse-header"></h6> --}}

      <a class="collapse-item" href="{{route('inventory.items.show')}}">Item List</a>

      {{-- <h6 class="collapse-header"></h6> --}}

      <a class="collapse-item" href="{{route('inventory.items.reject.list')}}">Rejected Items List</a>

    </div>
  </div>


</li>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item {{ activeMenu('accounts') }}">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#accountscollapse" aria-expanded="true" aria-controls="accountscollapse">
    <i class="fa fa-folder text-white"></i>
    <span>Accounts</span>
  </a>
  <div id="accountscollapse" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{route('accounts.expenses.show')}}">Expenses</a>

      <a class="collapse-item" href="{{route('accounts.revenue.show')}}">Revenue</a>

      <a class="collapse-item" href="{{route('accounts.dues.show')}}">Dues</a>

    </div>
  </div>


</li>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item {{ activeMenu('employee') }}">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#employeecollapse" aria-expanded="true" aria-controls="employeecollapse">
    <i class="fa fa-users text-white"></i>
    <span>Employee</span>
  </a>
  <div id="employeecollapse" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
     

      <a class="collapse-item" href="{{route('employee.show')}}">Stuff List</a>
   
     
    </div>
  </div>


</li>
<!-- Divider -->
<!-- <hr class="sidebar-divider">
 -->
<!-- Heading -->
<!-- <div class="sidebar-heading">
  Addons
</div>
 -->
<!-- Nav Item - Pages Collapse Menu -->
<!-- <li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
    <i class="fas fa-fw fa-folder"></i>
    <span>Pages</span>
  </a>
  <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">Login Screens:</h6>
      <a class="collapse-item" href="login.html">Login</a>
      <a class="collapse-item" href="register.html">Register</a>
      <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
      <div class="collapse-divider"></div>
      <h6 class="collapse-header">Other Pages:</h6>
      <a class="collapse-item" href="404.html">404 Page</a>
      <a class="collapse-item" href="blank.html">Blank Page</a>
    </div>
  </div>
</li> -->

<!-- Nav Item - Charts -->
<!-- <li class="nav-item">
  <a class="nav-link" href="charts.html">
    <i class="fas fa-fw fa-chart-area"></i>
    <span>Charts</span></a>
</li> -->

<!-- Nav Item - Tables -->
<!-- <li class="nav-item">
  <a class="nav-link" href="tables.html">
    <i class="fas fa-fw fa-table"></i>
    <span>Tables</span></a>
</li> -->

<!-- Divider -->
<!-- <hr class="sidebar-divider d-none d-md-block"> -->

<!-- Sidebar Toggler (Sidebar) -->
<!-- <div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div> -->

</ul>
<!-- End of Sidebar -->
