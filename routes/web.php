<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
    return redirect('/franchise/login');
});
 
// Admin prefix start
Route::prefix('admin')->group(function () {

// admin registration start
Route::get('/register','AdminAuthController@RegisterView');
Route::post('/register','AdminAuthController@RegisterSubmit')->name('admin.register');
// admin registration end

//admin login start
Route::get('/login','AdminAuthController@LoginView');
Route::post('/login','AdminAuthController@LoginSubmit')->name('admin.login');
//admin login end
 
// admin middleware start
    Route::group(['middleware' => 'adminsession'], function () {
    //Route::get('/dashboard', 'AdminAuthController@Dashboard');
    Route::get('/logout', 'AdminAuthController@Logout' )->name('admin.logout');
    Route::get('/franchise/create','FranchiseAuthController@FranchiseView');
    Route::post('/franchise/submit','FranchiseAuthController@FranchiseSubmit')->name('franchise.create');
    Route::get('/franchise/list','FranchiseAuthController@FranchiseTable');
    Route::get('/franchise/block/{user_name}', 'FranchiseAuthController@block' );
    Route::post('/franchise/list/update/{user_name}', 'FranchiseAuthController@update' )->name('franchise.update');
    Route::get('/franchise/documents/delete/{id}', 'FranchiseAuthController@DocsDelete');
    Route::post('/franchise/documents/upload/{user_name}', 'FranchiseAuthController@DocsUpload')->name('franchise.documents.upload');
    Route::get('/franchise/documents/list/{user_name}','FranchiseAuthController@DocsList');
    Route::get('/franchise/delete/{user_name}', 'FranchiseAuthController@franchiseDelete' );
    
     //student list
    Route::get('/student/list','StudentAuthController@studentViewList')->name('students.list');
    Route::post('/student/list/filterByName','StudentAuthController@studentListByName')->name('students.list.filterName');
    Route::post('/student/list/sort','StudentAuthController@studentListSort')->name('students.list.filterSort');
    Route::post('/student/list/searchByDate','StudentAuthController@studentListByDate')->name('students.list.filterDate');
    
    //course start
    Route::get('/course/create', 'CourseController@CourseView');
    Route::post('/course/create', 'CourseController@CourseSubmit')->name('create.course');
    //bulk upload
    Route::post('/course/create/bulk', 'CourseController@BulkImport')->name('import.course');
    Route::get('/course/list', 'CourseController@CourseList')->name('course.list');
    Route::get('/course/block/{course_code}', 'CourseController@block' );
    Route::get('/course/edit/{id}', 'CourseController@editCourseInfo')->name('edit.course.info');
    Route::post('/course/update', 'CourseController@updateCourseInfo')->name('update.course.info');
    Route::get('/course/delete/{id}', 'CourseController@deleteCourseInfo')->name('delete.course.info');
    
    //course end
    Route::get('/coupon/create', 'CouponController@CouponView');
    Route::post('/coupon/create', 'CouponController@CouponSubmit')->name('create.coupon');
    Route::get('/coupon/list/non-categories', 'CouponController@CouponListNonCategories');
    Route::post('/coupon/list/update', 'CouponController@CouponUpdate' )->name('coupon.update');
    Route::get('/coupon/block/{coupon_code}', 'CouponController@block' );
    Route::get('/coupon/delete/{coupon_code}', 'CouponController@couponDelete' );
    Route::get('/coupon/list/view/categories', 'CouponController@CouponListCategoriesView');
    Route::post('/coupon/list/categories', 'CouponController@CouponCategoriesList');

       //inventory items list
    Route::get('/inventory/items/showAll','InventoryController@showItem')->name('inventory.item.showAll');
    Route::post('/inventory/items/show','InventoryController@showItemByFranchise')->name('inventory.item.showByFranchise');

    });
// admin middleware end
});
// Admin prefix end



//Franchise prefix start
Route::prefix('franchise')->group(function () {

//franchise login start
Route::get('/login','FranchiseAuthController@LoginView');
Route::post('/login','FranchiseAuthController@LoginSubmit')->name('franchise.login');
//franchise login end
 
// franchise middleware start
Route::group(['middleware' => 'franchisesession'], function () {
    //Route::get('/dashboard','FranchiseAuthController@Dashboard');
    Route::get('/course/list','CourseController@franchise_CourseList')->name('franchise.course.list');
    Route::post('/student/submit','CourseController@courseEnrollNewStudent')->name('student.course.submit');
    Route::get('/student/create/{course_code}','CourseController@course_registration_view')->name('student.course.create');
    Route::get('/student/list','StudentAuthController@studentListByFranchise')->name('student.list');
    Route::get('/student/block/{user_name}', 'StudentAuthController@block' );
    Route::post('/student/update', 'StudentAuthController@updateStudentInformation')->name('student.info.update');
    Route::get('/student/delete/{id}', 'StudentAuthController@StudentDelete' );
    Route::get('/inventory/categories/view','InventoryController@viewCategory')->name('inventory.category.view');
    Route::post('/inventory/categories/create', 'InventoryController@createCategory')->name('inventory.category.create');
    Route::post('/inventory/item/category/create', 'InventoryController@createCategory')->name('inventory.item.category.create');

    Route::get('/inventory/categories/list','InventoryController@listCategory')->name('inventory.category.list');
    Route::get('/inventory/category/delete/{code}','InventoryController@deleteCategory')->name('inventory.category.delete');

    Route::get('/inventory/items/show','InventoryController@showItems')->name('inventory.items.show');
    Route::post('/inventory/items/create','InventoryController@createItem')->name('inventory.items.create');
    Route::post('/inventory/items/search','InventoryController@searchItems')->name('inventory.items.search');
    Route::get('/inventory/items/delete/{code}','InventoryController@deleteItems')->name('inventory.items.delete');
    Route::post('/inventory/items/update','InventoryController@updateItems')->name('inventory.items.update');
    Route::post('/inventory/items/rejected','InventoryController@rejectedItems')->name('inventory.items.rejected');
    Route::get('/inventory/items/reject/list','InventoryController@rejectedItemsList')->name('inventory.items.reject.list');
    
    Route::post('/student/course/payment','CourseController@coursePaymentTransaction')->name('student.course.payment');
    Route::post('/old_student/course/payment','CourseController@coursePaymentTransactionForOldStudent')->name('old.student.course.enroll');


    Route::post('/expenses','AccountsController@storeExpenses')->name('franchise.expenses.create');
    Route::get('/accounts','AccountsController@showExpenses')->name('accounts.expenses.show');
    Route::post('/expenses/list/rangedate','AccountsController@expensesListByRangedate')->name('expenses.list.rangedate');
    Route::post('/expenses/edit','AccountsController@editExpenses')->name('franchise.expenses.edit');


    Route::get('/revenue','AccountsController@getRevenueByFranchiseCode_ShowPage')->name('accounts.revenue.show');
    Route::get('/revenue/range/date','AccountsController@getRevenueByFranchiseCode_RangeDate')->name('revenue.list.rangedate');

    Route::get('/registration/dues','AccountsController@showDuesData')->name('accounts.dues.show');

    Route::post('/registration/dues/pay','AccountsController@dueCoursePayments')->name('accounts.dues.pay');
    Route::get('/registration/dues/range','AccountsController@getCourseRegDuesByFranchiseCode_RangeDate')->name('courseregdue.list.rangedate');

    Route::get('/employee','EmployeeController@showEmployee')->name('employee.show');
    Route::post('/employee/save','EmployeeController@saveEmployee')->name('franchise.employee.create');
    Route::post('/employee/edit','EmployeeController@editEmployee')->name('franchise.employee.edit');

    
    Route::get('/logout', 'FranchiseAuthController@fLogout' )->name('franchise.logout');
    });
// franchise middleware enstudent/d
});
//Franchise prefix end



//Student prefix start
Route::prefix('student')->group(function () {

//student login start
Route::get('/login','StudentAuthController@LoginView');
Route::post('/login','StudentAuthController@LoginSubmit')->name('student.login');
//student login end
//testing help
Route::view('/dashboard','welcome');
});
//Student prefix end




// Arpita added
Route::get('/profile', function () {
    return view('thinkdashboard.admin.pages.admin-personal.admin-profile');
});