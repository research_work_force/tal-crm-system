<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('u_name')->unique();
            $table->string('email');
            $table->string('name');
            $table->bigInteger('contact');
            $table->string('location');
            $table->string('g_name');
            $table->bigInteger('g_contact');
            $table->string('g_email');
            $table->string('clg_name');
            $table->string('future_goal');
            $table->string('course_name');
            $table->string('course_duration');
            $table->string('pan');
            $table->string('reg_card');
            $table->string('photo');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
