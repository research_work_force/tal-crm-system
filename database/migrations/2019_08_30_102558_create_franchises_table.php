<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFranchisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('franchises', function (Blueprint $table) {
            $table->increments('franchise_id');
            $table->string('name');
            $table->string('email');
            $table->string('user_name')->unique();
            $table->bigInteger('phone');
            $table->string('location');
            $table->date('agreement_date');
            $table->string('pf_esi')->nullable();
            $table->bigInteger('oficial_number')->nullable();
            $table->string('franchise_account_manager');
            $table->string('trade_license');
            $table->string('ifsc_code');
            $table->string('bank_name');
            $table->string('branch_name');
            $table->string('pan');
            $table->string('tan')->nullable();
            $table->string('gst')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('franchises');
    }
}
