<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RejectedItems extends Model
{
    protected $table = 'inventory_rejected_items';
}
