<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DueCoursePayments extends Model
{
    protected $table = 'due_course_payments';
}
