<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentModeTypes extends Model
{
   protected $table = 'mode_of_payment';
}
