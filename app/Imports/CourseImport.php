<?php

namespace App\Imports;

use App\Course;
use Maatwebsite\Excel\Concerns\ToModel;
// use Maatwebsite\Excel\Concerns\WithStartRow;

class CourseImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Course([
            'coupon_code'            => $row[0],
            'coupon_name'            => $row[1],
            'discount_percentage'    => $row[2],
            'discount_price'         => $row[3],
            'course_code'            => $row[4],
            'fran_name'              => $row[5],
        ]);
    }
}
