<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryItems extends Model
{
    protected $table = 'inventory_items';
}
