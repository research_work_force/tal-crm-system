<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expenses;
use App\CourseTransaction;
use App\Course;
use App\DueCoursePayments;
use App\Payments;
use Carbon\Carbon;

class AccountsController extends Controller
{

    public function showExpenses()
    { 
    	$expenses_data = Expenses::where('added_by',\Session::get('franchisesession'))
    	                           ->orderBy('created_at', 'desc')
    	                           ->paginate(10);

    	return view('thinkdashboard.franchise.pages.accounts.expensesList')
    	        ->with('expenses_data', $expenses_data);
    }

    public function storeExpenses(Request $request)
    {
    	$date = $request->input('date');
    	$amount = $request->input('amount');
    	$statement = $request->input('statement');
    	$added_by = \Session::get('franchisesession');

    	$expenses_data = new Expenses();
    	$expenses_data->statement = $statement;
    	$expenses_data->amount = $amount;
    	$expenses_data->added_by = $added_by;
    	$expenses_data->date = $date;
    	$expenses_data->save();

    	\Session::flash('message', "Expenses has been updated");
        \Session::flash('alert-class', 'alert-success');

        return $this->showExpenses();
    }

    public function expensesListByRangedate(Request $request)
    {
    	$fdate = $request->input('fdate');
    	$tdate = $request->input('tdate');
    	$added_by = \Session::get('franchisesession');

    	$expenses_daterange_data = Expenses::where('added_by',$added_by)
    										  ->whereBetween('date', [$fdate, $tdate])
    										  ->orderBy('created_at', 'desc')
    										  ->paginate(10);
    	return view('thinkdashboard.franchise.pages.accounts.expensesList')
    	        ->with('expenses_data', $expenses_daterange_data);
    }

    public function editExpenses(Request $request)
    {
    	$date = $request->input('date');
    	$amount = $request->input('amount');
    	$statement = $request->input('statement');
    	$id    = $request->input('id');
    	$added_by = \Session::get('franchisesession');

    	$checking_expenses_data = Expenses::where('added_by',$added_by)
    										->where('id',$id)
    										->count();
    	if($checking_expenses_data == '1'){
           
           $expenses_edit_data = Expenses::find($id);
           $expenses_edit_data->date = $date;
           $expenses_edit_data->amount = $amount;
           $expenses_edit_data->statement = $statement;
           $expenses_edit_data->save();

           \Session::flash('message', "Updated Succesfully");
	       \Session::flash('alert-class', 'alert-success');

	        return $this->showExpenses();


    	}else{

    		\Session::flash('message', "Something went wrong");
	        \Session::flash('alert-class', 'alert-warning');

	        return $this->showExpenses();

    	}
    }

    public function getRevenueByFranchiseCode_ShowPage(){

        $f_session = \Session::get('franchisesession');

        $course_transaction_data = CourseTransaction::where('f_username',\Session::get('franchisesession'))
                                   ->orderBy('created_at', 'desc');

        
        $course_transaction_data = \DB::table('course-transaction')
            ->where('f_username',$f_session)
            ->join('payments', 'payments.id', '=', 'course-transaction.payment_id')
            ->join('courses', 'courses.course_code', '=', 'course-transaction.course_code')
            ->select('course-transaction.*', 'payments.*', 'courses.*')
            ->orderBy('course-transaction.created_at', 'desc')
            ->paginate(10);

            // print_r($course_transaction_data); die;        

        return view('thinkdashboard.franchise.pages.accounts.revenueList')
                ->with('course_transaction_data', $course_transaction_data);


    }

    public function getRevenueByFranchiseCode_RangeDate(Request $request){

        $fdate = $request->input('fdate');
        $tdate = $request->input('tdate');

        $f_session = \Session::get('franchisesession');

        $course_transaction_data = CourseTransaction::where('f_username',\Session::get('franchisesession'))
                                   ->orderBy('created_at', 'desc');

        
        $course_transaction_data = \DB::table('course-transaction')
            ->where('f_username',$f_session)
            ->whereBetween('course-transaction.date', [$fdate, $tdate])
            ->join('payments', 'payments.id', '=', 'course-transaction.payment_id')
            ->join('courses', 'courses.course_code', '=', 'course-transaction.course_code')
            ->select('course-transaction.*', 'payments.*', 'courses.*')
            ->orderBy('course-transaction.created_at', 'desc')
            ->paginate(10);
      

        return view('thinkdashboard.franchise.pages.accounts.revenueList')
                ->with('course_transaction_data', $course_transaction_data);


    }


    public function showDuesData()
    {
        $f_session = \Session::get('franchisesession');

        $course_transaction_data = CourseTransaction::where('f_username',\Session::get('franchisesession'))
                                   ->orderBy('created_at', 'desc');

        
        $course_transaction_data = \DB::table('course-transaction')
            ->where('f_username',$f_session)
            ->join('payments', 'payments.id', '=', 'course-transaction.payment_id')
            ->join('courses', 'courses.course_code', '=', 'course-transaction.course_code')
            ->select('course-transaction.*', 'payments.*', 'courses.*')
            ->orderBy('course-transaction.created_at', 'desc')
            ->paginate(10);

            // print_r($course_transaction_data); die;        

        return view('thinkdashboard.franchise.pages.accounts.registrationDuesList')
                ->with('course_transaction_data', $course_transaction_data);
    }


    public function dueCoursePayments(Request $request){
    
           $student_code = $request->input('student_code');
           $payment_id = $request->input('payment_id');
           $amount = $request->input('amount');

           //verify proper payment transaction statement

           $verify_payment_transaction = Payments::where('id',$payment_id)
                                                   ->first();

           $verify_payment_course_transaction = CourseTransaction::where('payment_id',$payment_id)
                                                                  ->where('student_code',$student_code)
                                                                  ->first();

            if(empty($verify_payment_transaction) && empty($verify_payment_course_transaction)){

                    \Session::flash('message', "Wrong payment transaction");
                    \Session::flash('alert-class', 'alert-danger');

                  return redirect()->back();

            }else{

                $due_amt = $verify_payment_transaction->amount_due;

                $amount_paid = intVal($due_amt) - intVal($amount);

                if($amount_paid <= 0){
                     
                     $payment_transaction_update = Payments::find($payment_id);
                     $payment_transaction_update->amount_due = '0';
                     $payment_transaction_update->amount_paid = intVal($verify_payment_transaction->amount_paid) + intVal($due_amt);

                     $payment_transaction_update->save();

                     $update_due_course_payment = new DueCoursePayments();
                     $update_due_course_payment->payment_id = $payment_id;
                     $update_due_course_payment->student_code = $student_code;
                     $update_due_course_payment->due_paid = $due_amt;
                     $update_due_course_payment->date = Carbon::now();
                     $update_due_course_payment->save();

                     \Session::flash('message', "Due paid");
                    \Session::flash('alert-class', 'alert-success');

                     return redirect()->back();


                }else {

                     $payment_transaction_update = Payments::find($payment_id);
                     $payment_transaction_update->amount_due = intVal($due_amt) - intVal($amount);
                     $payment_transaction_update->amount_paid = intVal($verify_payment_transaction->amount_paid) + intVal($amount);

                     $payment_transaction_update->save();

                     $update_due_course_payment = new DueCoursePayments();
                     $update_due_course_payment->payment_id = $payment_id;
                     $update_due_course_payment->student_code = $student_code;
                     $update_due_course_payment->due_paid = $due_amt;
                     $update_due_course_payment->date = Carbon::now();
                     $update_due_course_payment->save();

                     \Session::flash('message', "Due paid");
                    \Session::flash('alert-class', 'alert-success');

                     return redirect()->back();


                }


            }



    }


    public function getCourseRegDuesByFranchiseCode_RangeDate(Request $request){

        $fdate = $request->input('fdate');
        $tdate = $request->input('tdate');

        $f_session = \Session::get('franchisesession');

        $course_transaction_data = CourseTransaction::where('f_username',\Session::get('franchisesession'))
                                   ->orderBy('created_at', 'desc');

        
        $course_transaction_data = \DB::table('course-transaction')
            ->where('f_username',$f_session)
            ->whereBetween('course-transaction.date', [$fdate, $tdate])
            ->join('payments', 'payments.id', '=', 'course-transaction.payment_id')
            ->join('courses', 'courses.course_code', '=', 'course-transaction.course_code')
            ->select('course-transaction.*', 'payments.*', 'courses.*')
            ->orderBy('course-transaction.created_at', 'desc')
            ->paginate(10);
      

        return view('thinkdashboard.franchise.pages.accounts.registrationDuesList')
                ->with('course_transaction_data', $course_transaction_data);


    }

}
