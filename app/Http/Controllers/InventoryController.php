<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ItemCategory;
use App\InventoryItems;
use App\Franchise;
use App\InventoryRejectedItems;
use App\RejectedItems;
class InventoryController extends Controller
{
    public function viewCategory(Request $request)
    {
    	return view('thinkdashboard.franchise.pages.category.viewCategories');
    }
 
     public static function categories()
    {
        $item_category_data = ItemCategory::where('added_by',\Session::get('franchisesession'))->get();
        return $item_category_data;
    }

    public function createCategory(Request $request)
    {
    	$category_name = $request->input('category_name');
    	$category_slug = strtolower($request->input('category_slug'));
    	$category_description = $request->input('category_description');
    	$added_by = \Session::get('franchisesession');

    	$item_category = ItemCategory::where('slug', $category_slug)->first();

    	if(empty($item_category)){

    		    $item_category_data = new ItemCategory();
    		    $item_category_data->category_name = $category_name;
    		    $item_category_data->slug = $category_slug;
    		    $item_category_data->description = $category_description;
    		    $item_category_data->added_by = $added_by;
    		    $item_category_data->save();
                
                \Session::flash('message', 'Category has been created');
                \Session::flash('alert-class', 'alert-info');

                return redirect()->back();
     	}
     	else{

     		    \Session::flash('message', 'Slug should be unique');
                \Session::flash('alert-class', 'alert-danger');

                return redirect()->back();
     	}
    }

    public function listCategory()
    {
    	$item_category_data = ItemCategory::where('added_by',\Session::get('franchisesession'))->orderBy('created_at', 'desc')->paginate(10);

    	return view('thinkdashboard.franchise.pages.category.listCategory')
    	             ->with('item_category_data', $item_category_data);
    }

    public function deleteCategory($category_id)
    {
    	$item_category_data = ItemCategory::find($category_id);
    	$item_category_data->delete();

    	       \Session::flash('message', 'Category has been deleted');
                \Session::flash('alert-class', 'alert-success');

                return redirect()->back();

    }

    public function showItems()
    {
    	$item_data = InventoryItems::where('added_by',\Session::get('franchisesession'))->orderBy('created_at', 'desc')->paginate(10);

  		$categories_data = ItemCategory::where('added_by',\Session::get('franchisesession'))->get();
    	return view('thinkdashboard.franchise.pages.items.itemList')
    	             ->with('categories_data',$categories_data)
    	             ->with('item_data', $item_data);

    }

    public function createItem(Request $request)
    {
    	$category_name = $request->input('category_name');
    	$item_name  = $request->input('item_name');
    	$item_quantity    = $request->input('item_quantity');
    	$item_amount = $request->input('item_amount');
        $item_description = $request->input('item_description');
        $added_by = \Session::get('franchisesession');

    	$item_data = new InventoryItems();
    	$item_data->item_name = $item_name;
    	$item_data->description = $item_description;
        $item_data->number_of_items = $item_quantity;
        $item_data->amount = $item_amount;
        $item_data->added_by = $added_by;
        $item_data->category_name = $category_name;
        $item_data->save();

        \Session::flash('message', 'Item has been added');
        \Session::flash('alert-class', 'alert-success');

        return redirect()->back();
   } 

   //admin can view inventory items
   public function showItem()
    {   
        

        $franchise_data = Franchise::orderBy('created_at','desc')->get();

/*        $categories_data = ItemCategory::orderBy('created_at','desc')->paginate(10);
*/        return view('thinkdashboard.admin.pages.items.item-list')
                ->with('franchise_data',$franchise_data);
                     /*->with('categories_data',$categories_data)
                     */
                     
                     

    }

    //admin inventory items by selected franchise
   public function showItemByFranchise(Request $request)
    {   

        $fran=$request->input('franchiseName');
        $fun=$request->input('itype');

        if($fun== "funtioning"){

            $franchise_data = Franchise::orderBy('created_at','desc')->paginate(10);            
             $categories_data = ItemCategory::orderBy('created_at','desc')->paginate(10);
              $item_data = InventoryItems::where('added_by',$fran)->paginate(10);
              return view('thinkdashboard.admin.pages.items.item-list-view')
                     ->with('categories_data',$categories_data)
                     ->with('item_data', $item_data)
                     ->with('franchise_data',$franchise_data);

           }  

           elseif($fun == "rejected"){

            $franchise_data = Franchise::orderBy('created_at','desc')->paginate(10);            
             $categories_data = ItemCategory::orderBy('created_at','desc')->paginate(10);
              $item_data = InventoryRejectedItems::where('added_by',$fran)->paginate(10);
              return view('thinkdashboard.admin.pages.items.item-list-view')
                     ->with('categories_data',$categories_data)
                     ->with('item_data', $item_data)
                     ->with('franchise_data',$franchise_data);


           }                      

    }

    public function updateItems(Request $request)
    {
        $category_name = $request->input('category_name');
        $item_name = $request->input('item_name');
        $item_quantity = $request->input('item_quantity');
        $item_amount =  $request->input('item_amount');
        $item_description = $request->input('item_description');
        $id = $request->input('id');
        $added_by = \Session::get('franchisesession');

        $item_check = InventoryItems::where('id', $id)->count();

        if($item_check == '1'){
            $item_data = InventoryItems::find($id);
            $item_data->category_name = $category_name;
            $item_data->item_name = $item_name;
            $item_data->number_of_items = $item_quantity;
            $item_data->amount = $item_amount;
            $item_data->description = $item_description;
            $item_data->added_by = $added_by;
            $item_data->save();

               \Session::flash('message', 'Item has been updated');
                \Session::flash('alert-class', 'alert-success');

                return redirect()->back();

        }else{

               \Session::flash('message', 'Something went wrong');
                \Session::flash('alert-class', 'alert-danger');

                return redirect()->back();
        }

    }

    public function deleteItems($code)
    {
        $item_data = InventoryItems::find($code);
        $item_data->delete();

         \Session::flash('message', 'Item has been deleted');
                \Session::flash('alert-class', 'alert-danger');

        return redirect(route('inventory.items.show'));
    }

    public function rejectedItemsList()
     {

        $item_data = RejectedItems::where('added_by',\Session::get('franchisesession'))->orderBy('created_at', 'desc')->paginate(10);

        return view('thinkdashboard.franchise.pages.items.rejectItemList')
                     ->with('item_data', $item_data);

     }


    public function rejectedItems(Request $request)
     {
 
            $id = $request->input('id');
            $item_quantity = $request->input('item_quantity');
            $remarks = $request->input('remarks');
              
            $item_data_active = InventoryItems::where('id', $id)->first();
            if($item_data_active != null){

                $old_number_of_items = $item_data_active->number_of_items;
                $item_name  = $item_data_active->item_name;
                $amount = $item_data_active->amount;
                $category_name = $item_data_active->category_name;
                $added_by = \Session::get('franchisesession');

                $rest_no_of_item = intVal($item_data_active->number_of_items) - intVal($item_quantity);


                      if($rest_no_of_item >= 0) 
                      {     
                            $item_updated_data = InventoryItems::find($id);
                            $item_updated_data->number_of_items = $rest_no_of_item;
                            $item_updated_data->save();



                         $rejectedItems_data = new RejectedItems();
                         $rejectedItems_data->item_name = $item_name;
                         $rejectedItems_data->remarks = $remarks;
                         $rejectedItems_data->no_of_items = $item_quantity;
                         $rejectedItems_data->amount = $amount;
                         $rejectedItems_data->category_name = $category_name;
                         $rejectedItems_data->added_by = $added_by;
                         $rejectedItems_data->save();
                      }
                      else {

                              \Session::flash('message', 'Reject quantity should not be more than its total quantity');
                              \Session::flash('alert-class', 'alert-danger');

                              return redirect()->back();

                      }


               \Session::flash('message', 'Item quantity has been updated');
                \Session::flash('alert-class', 'alert-success');

                return redirect()->back();

            }else{

        
                 \Session::flash('message', 'Something went wrong');
                \Session::flash('alert-class', 'alert-danger');

                return redirect()->back();
            }
            
           



     }

     public function searchItems(Request $request)
     {
        $category_name = $request->input('category_name');
        $item_name = $request->input('item_name');
         
        if(empty($item_name)){

                 $search_items_data = InventoryItems::where('category_name', 'LIKE', '%' . $category_name . '%')
                                             ->where('added_by',\Session::get('franchisesession'))
                                             ->orderBy('created_at','desc')
                                             ->paginate(10);

        }elseif(empty($category_name)){

                 $search_items_data = InventoryItems::Where('item_name', 'LIKE', '%' . $item_name . '%')
                                             ->where('added_by',\Session::get('franchisesession'))
                                             ->orderBy('created_at','desc')
                                             ->paginate(10);

        }elseif(empty($category_name) && empty($item_name)){

          $search_items_data = InventoryItems::where('added_by',\Session::get('franchisesession'))
                                             ->orderBy('created_at','desc')
                                             ->paginate(10);
        }else{

           $search_items_data = InventoryItems::where('added_by',\Session::get('franchisesession'))
                                             ->orderBy('created_at','desc')
                                             ->paginate(10);

        }
        

        $categories_data = ItemCategory::where('added_by',\Session::get('franchisesession'))->get();
        
        return view('thinkdashboard.franchise.pages.items.itemList')
                   ->with('categories_data',$categories_data)
                   ->with('item_data', $search_items_data);
     } 
           


}
