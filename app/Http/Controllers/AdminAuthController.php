<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminAuthController extends Controller
{
    //Admin Registration Start
    public function RegisterView(){
        return view('thinkdashboard.admin.pages.admin-personal.signup');
        }


    public function RegisterSubmit(Request $request){


        // validation checker
        $rules = [
                'firstname' => 'required|max:255',
                'lastname' => 'required|max:255',
                'email' => 'required|unique:admins|unique:logins|max:255',
                'address' => 'required|max:255',
                'contact' => 'required|digits:10',
                'password' => 'required|confirmed|min:6',
            ];
            $error_massage = [
                'firstname.required' => 'First Name Is Required',
                'firstname.max:255' => 'First Name Should Be 255 Character Maximum',
                'lastname.required' => 'Last Name Is Required',
                'lastname.max:255' => 'Last Name Should Be 255 Character Maximum',
                'email.required' => 'Email Address Is Required',
                'email.max:255' => 'Email Address Should Be 255 Character Maximum',
                'email.unique:admins' => 'Email Address Shoud Be Unique',
                'email.unique:logins' => 'Email Address Shoud Be Unique',
                'address.required' => 'Address Is Required',
                'address.max:255' => 'Address Should Be 255 Character Maximum',
                'contact.required' => 'Phone Number Is Required',
                'contact.max:255' => 'Phone Number Should Be 10 Digits',
                'password.required' => 'Password Is Required',
                'password.confirmed' => 'Password Should be Match with Confirm Password',
                'password.min:6' => 'Password Should Be Greater Than 6 Character ',
            ];
            $this->validate($request, $rules, $error_massage);

            //save in database of admin
         $admin = Admin::where("email", $request->input('email'))->first();

         if(empty($admins)) {
             $admin = new Admin();
             $admin->fname = $request->input('firstname');
             $admin->lname = $request->input('lastname');
             $admin->email = $request->input('email');
             $admin->address = $request->input('address');
             $admin->phone = $request->input('contact');
             $admin->save();

             $login_user = new Login();
             $login_user->email = $request->input('email');
             $login_user->password = Hash::make($request->input('password'));
             $login_user->save();


             \Session::flash('message', "Account has been created");
             \Session::flash('alert-class', 'alert-success');

              return redirect('/admin/login');
            }


             else{

                \Session::flash('message', "Sorry You Can't create an Account");
                return redirect()->back();
               }

        }
    //Admin Registration End


        //Admin Login Start
    public function LoginView(){
        return view('thinkdashboard.admin.pages.admin-personal.login');
        }

    public function LoginSubmit(Request $request){

            // validation checker
        $validatedData = $request->validate([
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
        ]);

        // save in variable
        $email = $request->input('email');
        $password = $request->input('password');

        //check in database of login
        $login_users = Login::where("email",$email)->first();

        if(empty($login_users)) {

            \Session::flash('message', "You enter wrong User Name");
            \Session::flash('alert-class', 'alert-danger');

            return back();
        }
        else{

            if(Hash::check($password,$login_users['password'])) {

                \Session::flash('message', "You are Loged In");
                \Session::flash('alert-class', 'alert-success');
                session(['adminsession' => $email]);
                return redirect('/admin/franchise/create');
            }
            else{
                \Session::flash('message', "You enter wrong Password");
                \Session::flash('alert-class', 'alert-danger');

                return back();

                }

            }
        }
        //Admin Login End




// admin Dashboard
    public function Dashboard()
    {
        return view('thinkdashboard.admin.pages.admin-personal.index');
    }





         // logout controller
         public function Logout(){
            \Session::flush();
            \Session::flash('message', "You are Loged Out");
            \Session::flash('alert-class', 'alert-danger');

            return redirect('/admin/login');
            }
        //logout controller




}
