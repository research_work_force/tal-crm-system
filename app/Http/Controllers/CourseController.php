<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Coupon;
use App\PaymentModeTypes;
use Storage;
use App\Imports\CourseImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\StudentAuthController;
use App\CourseTransaction;
use App\Payments;
use App\Student;
use Carbon\Carbon;

class CourseController extends Controller
{
    public function random_num($size) {
            $alpha_key = '';
            $keys = range('A', 'Z');
            
            for ($i = 0; $i < 2; $i++) {
                $alpha_key .= $keys[array_rand($keys)];
            }
            
            $length = $size - 2;
            
            $key = '';
            $keys = range(0, 9);
            
            for ($i = 0; $i < $length; $i++) {
                $key .= $keys[array_rand($keys)];
            }
            
            return $alpha_key . $key;
        }


    //Course Create Form Start
    public function CourseView(){

        return view('thinkdashboard.admin.pages.course.course-creation');
        }
    //Course Create Form End

    //Course Submit Start
    public function CourseSubmit(Request $request)
    {
        $request->validate([
            'course_code' => 'required|alpha_num|unique:courses',
            'course_name' => 'required|',
            'course_description' => 'required',
            'course_duration' => 'required|numeric',
            'course_fees' => 'required|numeric',
            'course_tags' => 'required',
            'thumbnail' => 'nullable|mimes:png,jpg,jpeg'

        ]);

        $course_code    = $request->input('course_code');
        $course_name    = $request->input('course_name');
        $course_description    =  $request->input('course_description');
        $course_duration    = $request->input('course_duration');
        $course_fees    = $request->input('course_fees');
        $course_tags    = $request->input('course_tags');

        $course_create = Course::where("course_code", $request->input('course_code'))->first();

        if(empty($coupons)) {

            $course_create = new Course();
            $course_create->course_code = $course_code;
            $course_create->course_name = $course_name;
            $course_create->course_description = $course_description;
            $course_create->course_duration = $course_duration;
            $course_create->course_fees = $course_fees;
            $course_create->course_tags = $course_tags;
            $course_create->status = "1";

            if ($request->file('thumbnail')) {

                    $thumb = $request->file('thumbnail');
    
                    $name=time().".".$thumb->getClientOriginalExtension();
                    $directory = 'upload/course/thumbnail/';

                    $res1 = $thumb->move($directory,$name);

            }
            else {

                $name = "thumbnail.png";

            }

            $course_create->thumbnail = $name;
            $course_create->save();

        }


        \Session::flash('message', "Course has been created");
        \Session::flash('alert-class', 'alert-success');
        return back();
    }

    public function BulkImport(Request $request)
    {


        if ($request->hasFile('file')) {
            $documents = $request->file('file');
            $directory = 'public/upload/documents'.'/';
            $filename = $directory.time() . '.' . $documents->getClientOriginalExtension();


            if(!is_dir($directory)){
                 Storage::makeDirectory($directory);
                 }

            $path = $request->documents->storeAs('', $filename);

            Excel::import(new CourseImport,$path);

            list($pub,$upload,$docu,$name)=explode("/",$filename);
            $docs = "storage/".$upload."/".$docu."/".$name;
            File::delete($docs);

            \Session::flash('message', "Bulk Upload Succesfully Complete");
            \Session::flash('alert-class', 'alert-info');
        return back();
    }

}
        public function CourseList()
        {
            $course = Course::orderBy('created_at', 'desc')->paginate(9);
            return view('thinkdashboard.admin.pages.course.course-list',compact('course'));
        }

            //block unblock
            public function block($course_code){
                $course_block = Course::where('course_code',$course_code)->first();

                if ($course_block->status == 1) {
                    $course_block->status = "0";
                }
                else {
                    $course_block->status = "1";
                }
                $course_block->save();

            return back();
            }

            public function franchise_CourseList()
        {
            $course = Course::orderBy('created_at', 'desc')->paginate(10);
            return view('thinkdashboard.franchise.pages.courses.courselist',compact('course'));
        }

 
// ------------------------

        public function editCourseInfo($id)
        {
            $course_data = Course::where('id', $id)->first();

            return view('thinkdashboard.admin.pages.course.course-edit')
                   ->with('course_data',$course_data);
        }

        public function updateCourseInfo(Request $request)
        {
             $request->validate([
            // 'course_code' => 'required|alpha_num|unique:courses',
            'course_name' => 'required|',
            'course_description' => 'required',
            'course_duration' => 'required|numeric',
            'course_fees' => 'required|numeric',
            'course_tags' => 'required',
            'thumbnail' => 'nullable|mimes:png,jpg,jpeg'

            ]);

            $course_code    = $request->input('course_code');
            $course_name    = $request->input('course_name');
            $course_description    =  $request->input('course_description');
            $course_duration    = $request->input('course_duration');
            $course_fees    = $request->input('course_fees');
            $course_tags    = $request->input('course_tags');
            $id    = $request->input('id');

            // $course_update = Course::where("course_code", $request->input('course_code'))->first();

            $course_update = Course::find($id);
            $course_update->course_name = $course_name;
            $course_update->course_description = $course_description;
            $course_update->course_duration = $course_duration;
            $course_update->course_fees = $course_fees;
            $course_update->course_tags = $course_tags;

            if ($request->file('thumbnail')) {

                    $thumb = $request->file('thumbnail');
    
                    $name=time().".".$thumb->getClientOriginalExtension();
                    $directory = 'upload/course/thumbnail/';

                    $res1 = $thumb->move($directory,$name);

                    $course_update->thumbnail = $name;

            }

            $course_update->save();

            return redirect(route('course.list'));
     }

     public function deleteCourseInfo($id)
     {
        $course_delete = Course::find($id);

        $course_delete->delete();

        return redirect()->back();
     }

     public function course_registration_view($course_code)
     {
         $course_data = Course::where('course_code',$course_code)->first();
         $coupon_data = Coupon::where('course_code',$course_code)
                                   ->where('franchise_user_name',\Session::get('franchisesession'))
                                   ->where('coupon_status','1')
                                   ->get();

        
         return view('thinkdashboard.franchise.pages.students.student_create')
                    ->with('course_data',$course_data)
                    ->with('coupon_data',$coupon_data);
     }

     public function courseEnrollNewStudent(Request $request)
     {
    // calling Student Controller for submit student information

        $student_data = new StudentAuthController();
        $student_data_status = $student_data->StudentSubmit($request);
        $course_code = $request->input('course_code');
        $coupon_code = $request->input('coupon_code_applied');

        $coupon_data = Coupon::where('course_code',$course_code)
                               ->where('coupon_code',$coupon_code)
                               ->where('franchise_user_name',\Session::get('franchisesession'))
                               ->where('coupon_status','1')
                               ->first();

        $course_data = Course::where('course_code',$course_code)
                               ->first();

        if($coupon_data != null && $course_data != null) {
                    $course_price = $coupon_data->discount_price;
                    $course_discount = $coupon_data->discount_percentage;
                    $amount_need_to_be_paid = intVal($course_data->course_fees) - intVal($course_price);

                    if($student_data_status != '0')
                    {
                        // Course information
                       
                        $course_data = Course::where('course_code',$course_code)
                                               ->first();

                        // Coupon information

                        $coupon_data = Coupon::where('course_code',$course_code)
                                           ->where('coupon_code',$coupon_code)
                                           ->where('franchise_user_name',\Session::get('franchisesession'))
                                           ->where('coupon_status','1')
                                           ->first();

                        // Payment mode types information

                         $payment_types = PaymentModeTypes::get();
                      
                        return view('thinkdashboard.franchise.pages.payments.payment-view')
                               ->with('student_code',$student_data_status)
                               ->with('amount_need_to_be_paid',$amount_need_to_be_paid)
                               ->with('coupon_data',$coupon_data)
                               ->with('payment_type',$payment_types)
                               ->with('course_data',$course_data);
                    }
                    else{

                          \Session::flash('message', "Something went wrong. Please try again!!!");
                          \Session::flash('alert-class', 'alert-danger');

                        return redirect()->back();
                    }
        }else{
                        \Session::flash('message', "Coupon Code is not available");
                        \Session::flash('alert-class', 'alert-warning');

                        return redirect()->back();
        }

     }

     public function coursePaymentTransaction(Request $request)
     {
        $date = Carbon::now();
        $student_code = $request->input('student_code');
        $amt_paid    = $request->input('amt_paid');
        $amt_due    = $request->input('amt_due');
        $mode_of_payment    = $request->input('mode_of_payment');
        $denominators    = $request->input('denominators');
        $cheque_number    = $request->input('cheque_number');
        $description    = $request->input('description');
        $coupon_code = $request->input('coupon_code');
        $course_code = $request->input('course_code');

        if($amt_due > 0){
            $due_status  = '1';
        }else{
            $due_status = '0'; 
        }
        

        $validate_student_course = Student::where('course_code',$course_code)
                                            ->where('student_code',$student_code)
                                            ->count();

        $validate_coupon_data = Coupon::where('course_code',$course_code)
                               ->where('coupon_code',$coupon_code)
                               ->where('coupon_status','1')
                               ->count();
       

        if($validate_coupon_data == '1')
        {
            $tal_payment_id = $this->random_num(18);

            $course_transaction_data = new CourseTransaction();
            $course_transaction_data->course_code = $course_code;
            $course_transaction_data->coupon_code = $coupon_code;
            $course_transaction_data->student_code = $student_code;
            $course_transaction_data->payment_id = $tal_payment_id;
            $course_transaction_data->status = '1';
            $course_transaction_data->date = $date;
            $course_transaction_data->f_username = \Session::get('franchisesession');
            $course_transaction_data->save();


            $course_payment_data = new Payments();
            $course_payment_data->id = $tal_payment_id;
            $course_payment_data->statement = $description;
            $course_payment_data->payment_type = $mode_of_payment;
            $course_payment_data->denomination = $denominators;
            $course_payment_data->cheque_number = $cheque_number;
            $course_payment_data->amount_paid = $amt_paid;
            $course_payment_data->amount_due = $amt_due;
            $course_payment_data->total_amount = intVal($amt_paid) + intVal($amt_due);
            $course_payment_data->due_status = $due_status;
            $course_payment_data->save();

                \Session::flash('message', "Course Enrollment done");
                \Session::flash('alert-class', 'alert-success');

            return redirect(route('franchise.course.list'));

            }else {

               \Session::flash('message', "Something went wrong");
                \Session::flash('alert-class', 'alert-warning');

            return redirect(route('franchise.course.list'));
            }
     }

     public function coursePaymentTransactionForOldStudent(Request $request)
     {
        $student_code = $request->input('student_code');
        $coupon_code = $request->input('coupon_code');
        $course_code    = $request->input('coursecode');

        $franchise_user_name = \Session::get('franchisesession');

            $validate_coupon = Coupon::where('franchise_user_name',$franchise_user_name)
                                    ->where('course_code',$course_code)
                                    ->where('coupon_code',$coupon_code)
                                    ->count();
            $validate_student = Student::where('student_code',$student_code)
                                    ->count();

         if($validate_coupon == '1' && $validate_student == '1'){


                                        // Coupon information

                                    $coupon_data = Coupon::where('course_code',$course_code)
                                                           ->where('coupon_code',$coupon_code)
                                                           ->where('franchise_user_name',\Session::get('franchisesession'))
                                                           ->where('coupon_status','1')
                                                           ->first();

                                    $course_data = Course::where('course_code',$course_code)
                                                           ->first();

                                    if($coupon_data != null && $course_data != null) {
                                    $course_price = $coupon_data->discount_price;
                                    $course_discount = $coupon_data->discount_percentage;
                                    $amount_need_to_be_paid = intVal($course_data->course_fees) - intVal($course_price);

                                  
                                        // Course information
                                       
                                        $course_data = Course::where('course_code',$course_code)
                                                               ->first();


                                        // $coupon_data = Coupon::where('course_code',$course_code)
                                        //                    ->where('coupon_code',$coupon_code)
                                        //                    ->where('franchise_user_name',\Session::get('franchisesession'))
                                        //                    ->where('coupon_status','1')
                                        //                    ->first();

                                        // Payment mode types information

                                         $payment_types = PaymentModeTypes::get();

                                          return view('thinkdashboard.franchise.pages.payments.payment-view')
                                               ->with('student_code',$student_code)
                                               ->with('amount_need_to_be_paid',$amount_need_to_be_paid)
                                               ->with('coupon_data',$coupon_data)
                                               ->with('payment_type',$payment_types)
                                               ->with('course_data',$course_data);

                                      }else{

                                           \Session::flash('message', "Coupon Code is not available");
                                            \Session::flash('alert-class', 'alert-warning');

                                        return redirect(route('franchise.course.list'));

                                      }

                                       

         }
         else{

             \Session::flash('message', "Coupon Code/Student Code is not matching. Try again.");
                \Session::flash('alert-class', 'alert-warning');

            return redirect(route('franchise.course.list'));
         }
     }



}
