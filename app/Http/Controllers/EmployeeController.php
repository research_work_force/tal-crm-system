<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expenses;
use App\Employees;

class EmployeeController extends Controller
{
    public function showEmployee()
    {
    	$employee_data = Employees::where('added_by',\Session::get('franchisesession'))
    	                           ->orderBy('created_at', 'desc')
    	                           ->paginate(10);

    	return view('thinkdashboard.franchise.pages.employee.employee-list')
    	        ->with('employee_data', $employee_data);
    }


    public function saveEmployee(Request $request)
    {
             
            $name = $request->input('name');
            $address = $request->input('address');            
            $contact_num = $request->input('contact_num');          
            $identity_proof = $request->file('identity_proof');
            $added_by = \Session::get('franchisesession');


            if($request->hasFile('identity_proof')){

            	      $emp_data_check = Employees::where('contact_number',$contact_num)
            	                                   ->where('added_by',\Session::get('franchisesession'))->first();

		            	    if(empty($emp_data_check)){

		            	      $filename = time() .'.'.$identity_proof->getClientOriginalExtension();
		            		  $directory = 'upload/empdoc';
						      $move_to_folder = $identity_proof->move($directory,$filename);


		                      $employee_data = new Employees();
		                      $employee_data->name = $name;
		                      $employee_data->address = $address;
		                      $employee_data->contact_number = $contact_num;
		                      $employee_data->identity_card = $filename;
		                      $employee_data->added_by = $added_by;
		                      $employee_data->save();

		                      \Session::flash('message', "Inserted Succesfully");
			                  \Session::flash('alert-class', 'alert-success');

			                  return redirect()->back();


		            	    }else{

		            	      \Session::flash('message', "Phone number should be unique for all employees");
			                  \Session::flash('alert-class', 'alert-danger');

			                  return redirect()->back();

		            	    }
		            		
		            		  

            }else{

            		  \Session::flash('message', "Identity proof should be uploaded");
	                  \Session::flash('alert-class', 'alert-warning');

	                  return redirect()->back();

            }
    }


    public function editEmployee(Request $request)
    {
    	$name = $request->input('name');
    	$address = $request->input('address');
    	$contact_num = $request->input('contact_num');
    	$identity_proof = $request->file('identity_proof');
    	$added_by = \Session::get('franchisesession');
    	$id = $request->input('id');

    	         if($request->hasFile('identity_proof')){

    	         		// $emp_data_check = Employees::where('contact_number',$contact_num)
            	   //                                 ->where('added_by',\Session::get('franchisesession'))->first();

		            	    if(empty($emp_data_check)){

		            	    	  $filename = time() .'.'.$identity_proof->getClientOriginalExtension();
			            		  $directory = 'upload/empdoc';
							      $move_to_folder = $identity_proof->move($directory,$filename);

		            	    		$emp_data = Employees::find($id);
		            	    		$emp_data->name = $name;
		            	    		$emp_data->address = $address;
		            	    		$emp_data->contact_number = $contact_num;
		            	    		$emp_data->identity_card = $filename;
		            	    		$emp_data->added_by = $added_by;
		            	    		$emp_data->save();

		            	    		\Session::flash('message', "Updated Succesfully");
					                  \Session::flash('alert-class', 'alert-success');
		            	    		return redirect()->back();
		            	    }else{


		            	    		 \Session::flash('message', "This Phone number is exist.");
					                  \Session::flash('alert-class', 'alert-danger');

					                  return redirect()->back();
		            	    		

		            	    }
    	         }else {

    	         	                $emp_data = Employees::find($id);
		            	    		$emp_data->name = $name;
		            	    		$emp_data->address = $address;
		            	    		$emp_data->contact_number = $contact_num;
		            	    		$emp_data->added_by = $added_by;
		            	    		$emp_data->save();


		            	    		\Session::flash('message', "Updated Succesfully");
					                  \Session::flash('alert-class', 'alert-success');

					                  return redirect()->back();

    	         }

    }
}
