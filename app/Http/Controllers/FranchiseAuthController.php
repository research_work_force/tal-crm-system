<?php

namespace App\Http\Controllers;

use App\UserLogin;
use App\Franchise;
use App\Trackdocuments;
use Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;

class FranchiseAuthController extends Controller
{

    //Franchise Create Form Start
    public function FranchiseView(){
        return view('thinkdashboard.admin.pages.franchise.franchisecreate');
        }
    //Franchise Create Form End

    //error massage function
    public function errorMessege()
    {
        $error_massage = [
            'name.required' => 'Name Is Required',
            'name.max:255' => 'Name Should Be 255 Character Maximum',

            'email.required' => 'Email Is Required',
            'email.email' => 'Email Should be in Email Format',
            'email.max:255' => 'Email Should Be 255 Character Maximum',

            'user_name.required' => 'User Name Is Required',
            'user_name.unique:franchises' => 'User Name Shoud Be Unique',
            'user_name.unique:user_logins' => 'User Name Shoud Be Unique',
            'user_name.max:255' => 'User Name Should Be 255 Character Maximum',

            'password.required' => 'Password Is Required',
            'password.min:6' => 'Password Should Be 6 Character Minimum',

            'phone.required' => 'Phone Number Is Required',
            'phone.digits:10' => 'Phone Number Should Be 10 Digits',

            'location.required' => 'Location Is Required',
            'location.max:255' => 'Location Should Be 255 Character Maximum',
 
            'agreement_date.required' => 'Agreemant Date Is Required',
            'agreement_date.date' => 'Agreemant Date Should be in Date Format',
            'agreement_date.max:255' => 'Agreemant Date Should Be 255 Character Maximum',


            'pf_esi.alpha_num' => 'PF or ESI number Should Be Combination Of Number & Letter',
            'pf_esi.min:12'=>'PF number should be 12 digits and ESI should be 17',
            'pf_esi.min:17'=>'PF number should be 12 digits and ESI should be 17',


            'oficial_number.digits:10'=>'Offilcial Phone Number Should be 10 Digits',


            'franchise_account_manager.required' => 'Franchise Manager\'s Name Is Required',
            'franchise_account_manager.max:255' => 'Franchise Manager\'s Name Should Be 255 Character Maximum',


            'trade_license.required' => 'Trade License Number Is Required',
            'trade_license.max:255' => 'Trade License Number Should Be 255 Character Maximum',
            'trade_license.alpha_num'=> 'Trade License number Should Be Combination Of Number & Letter',


            'ifsc_code.required' => 'IFSC Code Is Required',
            'ifsc_code.min:11' => 'IFSC Code Should Be 11 Character',
            'ifsc_code.max:11' => 'IFSC Code Should Be 11 Character',
            'ifsc_code.alpha_num'=> 'IFSC Code Should Be Combination Of Number & Letter',


            'bank_name.required' => 'Bank Name Is Required',
            'bank_name.max:255' => 'Bank Name Should Be 255 Character Maximum',


            'branch_name.required' => 'Branch Name Is Required',
            'branch_name.max:255' => 'Branch Name Should Be 255 Character Maximum',


            'pan.required' => 'PAN Number Is Required',
            'pan.min:10' => 'PAN Number Should Be 10 Character',
            'pan.max:10' => 'PAN Number Should Be 10 Character',
            'pan.alpha_num'=> 'PAN Number Should Be Combination Of Number & Letter',


            'tan.required' => 'TAN Number Is Required',
            'tan.min:10' => 'TAN Number Should Be 10 Character',
            'tan.max:10' => 'TAN Number Should Be 10 Character',
            'tan.alpha_num'=> 'TAN Number Should Be Combination Of Number & Letter',


            'gst.required' => 'GST Number Is Required',
            'gst.min:15' => 'GST Number Should Be 15 Character',
            'gst.max:15' => 'GST Number Should Be 15 Character',
            'gst.alpha_num'=> 'GST Number Should Be Combination Of Number & Letter',


            'documents.required' => 'Please Upload Document',
            'documents.mimes:pdf' => 'Uploaded Document Should Be pdf format',
            ];

            return $error_massage;
    }
    //error messege end


    //Franchise Create Form Submit Start
    public function FranchiseSubmit(Request $request){


        // validation checker start
        $rules = [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255',
                'user_name' => 'required|unique:franchises|unique:user_logins|max:255',
                'password' => 'required|min:6',
                'phone' => 'required|digits:10',
                'location' => 'required|max:255',
                'agreement_date' => 'required|date|max:255',
                'pf_esi' => 'nullable|alpha_num|min:12|max:17',
                'oficial_number' => 'nullable|digits:10',
                'franchise_account_manager' => 'required|max:255',
                'trade_license' => 'required|alpha_num|max:255',
                'ifsc_code' => 'required|alpha_num|min:11|max:11',
                'bank_name' => 'required|max:255',
                'branch_name' => 'required|max:255',
                'pan' => 'required|alpha_num|min:10|max:10',
                'tan' => 'nullable|alpha_num|min:10|max:10',
                'gst' => 'nullable|alpha_num|min:15|max:15',
                'documents' => 'required',
                'documents.*' => 'mimes:pdf,jpeg,png,jpg',
                ];
            $error =$this->errorMessege();
            $this->validate($request, $rules, $error);
        // validation checker end

             $user_name=$request->input('user_name');
            //save in database of admin
         $user_fran = Franchise::where("user_name", $user_name)->first();

         if(empty($franchises)) {
             $user_fran = new Franchise();
             $user_fran->name =$request->input('name');
             $user_fran->email = $request->input('email');
             $user_fran->user_name = $user_name;
             $user_fran->phone = $request->input('phone');
             $user_fran->location = $request->input('location');
             $user_fran->agreement_date = $request->input('agreement_date');
             $user_fran->pf_esi = $request->input('pf_esi');
             $user_fran->oficial_number = $request->input('oficial_number');
             $user_fran->franchise_account_manager = $request->input('franchise_account_manager');
             $user_fran->trade_license = $request->input('trade_license');
             $user_fran->ifsc_code = $request->input('ifsc_code');
             $user_fran->bank_name = $request->input('bank_name');
             $user_fran->branch_name = $request->input('branch_name');
             $user_fran->pan = $request->input('pan');
             $user_fran->tan = $request->input('tan');
             $user_fran->gst = $request->input('gst');
             $user_fran->status = "1";
             $user_fran->save();
 

             if ($request->hasFile('documents')) {
                foreach($request->file('documents') as $file){

                    $name=$file->getClientOriginalName();
                    $directory = 'upload/documents/'.$user_name.'/';

                    $res1 = $file->move($directory,$name);

                    $track_docu= new Trackdocuments();
                    $track_docu->franchise_user_name = $user_name;
                    $track_docu->documents = $name;
                    $track_docu->save();
                    }
                }
            


             $login_user_fran = new UserLogin();
             $login_user_fran->user_name = $request->input('user_name');
             $login_user_fran->password = Hash::make($request->input('password'));
             $login_user_fran->type = "franchise";
             $login_user_fran->save();



             \Session::flash('message', "Account has been created");
             \Session::flash('alert-class', 'alert-success');

              return redirect()->back();
            }


             else{

                \Session::flash('message', "Sorry You Can't create an Account");
                return redirect()->back();
               }
        }
        //Franchise Create Form Submit End


            //Franchise list start
            public function FranchiseTable(){
                $users = Franchise::orderBy('created_at', 'desc')->paginate(10);

                foreach ($users as $user) {
                     $franchise_user_name = $user->user_name;
                     }
                     $documents = Trackdocuments::get();


                    return view('thinkdashboard.admin.pages.franchise.franchise-list', compact('users','documents'));
                }

                //block unblock
            public function block($user_name){
                $fran_login = Franchise::where('user_name',$user_name)->first();

                if ($fran_login->status == 1) {
                    $fran_login->status = "0";
                }
                else {
                    $fran_login->status = "1";
                }
                $fran_login->save();

            return back();
            }

                public function update(Request $request, $user_name)
                {

                    // validation checker start
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|digits:10',
            'location' => 'required|max:255',
            'agreement_date' => 'required|date|max:255',
            'pf_esi' => 'nullable|alpha_num|min:12|max:17',
            'oficial_number' => 'nullable|digits:10',
            'franchise_account_manager' => 'required|max:255',
            'trade_license' => 'required|alpha_num|max:255',
            'ifsc_code' => 'required|alpha_num|min:11|max:11',
            'bank_name' => 'required|max:255',
            'branch_name' => 'required|max:255',
            'pan' => 'required|alpha_num|min:10|max:10',
            'tan' => 'nullable|alpha_num|min:10|max:10',
            'gst' => 'nullable|alpha_num|min:15|max:15',
            ];
        $error =$this->errorMessege();
        $this->validate($request, $rules, $error);
    // validation checker end
             $fran = Franchise::where('user_name',$user_name)->first();

             $fran->name = $request->input('name');
             $fran->email = $request->input('email');
             $fran->phone = $request->input('phone');
             $fran->location = $request->input('location');
             $fran->agreement_date = $request->input('agreement_date');
             $fran->pf_esi = $request->input('pf_esi');
             $fran->oficial_number = $request->input('oficial_number');
             $fran->franchise_account_manager = $request->input('franchise_account_manager');
             $fran->trade_license = $request->input('trade_license');
             $fran->ifsc_code = $request->input('ifsc_code');
             $fran->bank_name = $request->input('bank_name');
             $fran->branch_name = $request->input('branch_name');
             $fran->pan = $request->input('pan');
             $fran->tan = $request->input('tan');
             $fran->gst = $request->input('gst');
             $fran->save();

                \Session::flash('message', "Account has been updated");
                \Session::flash('alert-class', 'alert-info');
                return redirect()->back();

                }


            //franchise documents update start

            public function DocsList($user_name)
            {

                $documents = Trackdocuments::get();

                return view('thinkdashboard.admin.pages.franchise.franchise-documents-update', compact('documents','user_name'));

            }

            public function DocsDelete($id)
            {
                $documents = Trackdocuments::where('id',$id)->first();
                $documents->delete();

                \Session::flash('message', 'The Documents details has been deleted');
                \Session::flash('alert-class', 'alert-danger');
                return redirect()->back();
            }

            public function docsUpload(Request $request,$user_name)
            {
             if ($request->hasFile('documents')) {

                foreach($request->file('documents') as $file){

                    $name=$file->getClientOriginalName();
                    $directory = 'public/upload/documents/'.$user_name.'/';
                    if(!is_dir($directory)){
                    Storage::makeDirectory($directory);
                    Storage::putFileAs($directory,$file,$name);

                    $track_docu= new Trackdocuments();
                    $track_docu->franchise_user_name = $user_name;
                    $track_docu->documents = $name;
                    $track_docu->save();
                    }
                }

            \Session::flash('message', 'The Documents details has been updated');
            \Session::flash('alert-class', 'alert-info');
            return redirect()->back();
            }
            else {
                \Session::flash('message', 'you are not choose any documents');
                \Session::flash('alert-class', 'alert-danger');
                return redirect()->back();
            }

            }


            //franchise documents update end

    public function franchiseDelete($user_name)
    {
                $fran = Franchise::where('franchise_id',$user_name)->first();
                $fran->status = '3';
                $fran->save();
                 
                $username_franchise = $fran->user_name;
                $fran_login = UserLogin::where('user_name',$username_franchise)->first();
                $fran_login->delete();

                \Session::flash('message', 'Franchise details has been deleted');
                \Session::flash('alert-class', 'alert-info');
                return redirect()->back();
    }
            //franchise update end

    public function Dashboard()
    {
        return view('thinkdashboard.franchise.pages.franchise-own.index');
    }

        //franchise login start
        public function LoginView(){
            return view('thinkdashboard.franchise.pages.franchise-own.login');
            }

        public function LoginSubmit(Request $request){

                // validation checker
            $validatedData = $request->validate([
                'user_name' => 'required|max:255',
                'password' => 'required|min:6',
            ]);

            // save in variable
            $user_name = $request->input('user_name');
            $password = $request->input('password');
            $hashpassword = Hash::make($password);

            //check in database of login
            $login_users = UserLogin::where("user_name",$user_name)->first();

            if(empty($login_users)) {

                \Session::flash('message', "You enter wrong User Name");
                \Session::flash('alert-class', 'alert-danger');

                return redirect('/franchise/login');
            }
            else{

                if(Hash::check($password,$login_users['password'])) {

                    //check if block
                    // if ($login_users->status == 1 | $login_users->type == "frachise") {

                        \Session::flash('message', "You are Loged In");
                        \Session::flash('alert-class', 'alert-success');
                        session(['franchisesession' => $user_name]);
                        return redirect('/franchise/course/list');
                    // }
                    // else {
                    //     \Session::flash('message', "You Are Unable to Login, Contact Your Administrator");
                    //     \Session::flash('alert-class', 'alert-danger');
                    //     return back();
                    // }

                }
                else{
                    \Session::flash('message', "You enter wrong Password");
                    \Session::flash('alert-class', 'alert-danger');

                    return redirect('/franchise/login');

                    }

                }
            }
        //franchise login end

            // logout controller
         public function fLogout(){
            \Session::flush();
            \Session::flash('message', "You are Loged Out");
            \Session::flash('alert-class', 'alert-danger');

            return redirect('/franchise/login');
            }
        //logout controller



    }
