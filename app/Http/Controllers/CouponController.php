<?php

namespace App\Http\Controllers;

use App\Course;
use App\Coupon;
use App\Franchise;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CouponController extends Controller
{
    //coupon Create Form Start
    public function CouponView(){

        $course =  Course::get();
        $fran = Franchise::get();



        return view('thinkdashboard.admin.pages.coupon.coupon-create', compact('course','fran'));
        }
    //coupon Create Form End

    //Course Submit Start
    public function CouponSubmit(Request $request)
    {

        $request->validate([
            'coupon_code' => 'required|alpha_num|unique:coupons',
            'coupon_description' => 'required',
            'discount_percentage' => 'required|numeric|min:1|max:100',
            'course_code' => 'required',
            'franchise_user_name' => 'required',
        ]);


        $coupon_save = Coupon::where("coupon_code", $request->input('coupon_code'))->first();

            $discount_percentage = $request->input('discount_percentage');
            $course = Course::where("course_code", $request->input('course_code'))->first();
            $course_price = $course->course_fees;
            $price = floatval($course_price*$discount_percentage*0.01);

         if(empty($coupons)) {
             $coupon_save = new Coupon();
             $coupon_save->coupon_code = $request->input('coupon_code');
             $coupon_save->coupon_description = $request->input('coupon_description');
             $coupon_save->discount_percentage = $discount_percentage;
             $coupon_save->discount_price = $price;
             $coupon_save->course_code = $request->input('course_code');
             $coupon_save->franchise_user_name = $request->input('franchise_user_name');
             $coupon_save->coupon_status = "1";
             $coupon_save->save();
         }


        \Session::flash('message', "Coupon has been created");
        \Session::flash('alert-class', 'alert-success');
        return back();
    }

    public function CouponListNonCategories()
    {
        $users = DB::table('coupons')
                ->join('courses','courses.course_code','coupons.course_code')
                ->join('franchises','franchises.user_name','coupons.franchise_user_name')
                ->orderBy('coupons.created_at', 'desc')
                ->paginate(10);


        $course =  Course::get();
        $fran = Franchise::get();

        return view('thinkdashboard.admin.pages.coupon.coupon-list', compact('users','course','fran'));
    }

       //block unblock
       public function block($coupon_code){
        $coupon_block = Coupon::where('coupon_code',$coupon_code)->first();

        if ($coupon_block->coupon_status == 1) {
            $coupon_block->coupon_status = "0";
        }
        else {
            $coupon_block->coupon_status = "1";
        }
        $coupon_block->save();

    return back();
    }
    //delete
    public function couponDelete($coupon_code)
    {
                $coupon = Coupon::where('coupon_code',$coupon_code)->first();
                $coupon->delete();

                \Session::flash('message', 'Franchise details has been deleted');
                \Session::flash('alert-class', 'alert-info');
                return redirect('admin/coupon/list/non-categories');
    }

    //update
    public function CouponUpdate(Request $request)
    {

        $request->validate([
            'coupon_code' => 'required|alpha_num',
            'coupon_description' => 'required',
            'discount_percentage' => 'required|numeric|min:1|max:100',
        ]);

       

        $discount_percentage = $request->input('discount_percentage');
        $course = Course::where("id", $request->input('course_id'))->first();
        $coupon_id = Coupon::where("course_code", $course->course_code)->first();

        $course_price = $course->course_fees;
        $price = floatval($course_price*$discount_percentage*0.01);
          // echo $course->course_code; 

          // echo $coupon_id->id;
          // die;

             $coupon_update = Coupon::find($coupon_id->id);
             $coupon_update->coupon_code = $request->input('coupon_code');
             $coupon_update->coupon_description = $request->input('coupon_description');
             $coupon_update->discount_percentage = $request->input('discount_percentage');
             $coupon_update->discount_price = $price;
             $coupon_update->save();
            // dd($request->input('coupon_code'));

        \Session::flash('message', "Coupon has been updated");
        \Session::flash('alert-class', 'alert-success');
        return redirect('admin/coupon/list/non-categories');
    }

    public function CouponListCategoriesView(){

        $course =  Course::orderBy('created_at', 'desc')->paginate(10);
        $fran = Franchise::orderBy('created_at', 'desc')->paginate(10);



        return view('thinkdashboard.admin.pages.coupon.coupon-categories', compact('course','fran'));
        }

        public function CouponCategoriesList(Request $request)
        {

            // $course_code = $request->input('course_code');
            $franchise_user_name = $request->input('franchise_user_name');

            $users =DB::table('coupons')
                ->join('courses','courses.course_code','coupons.course_code')
                ->join('franchises','franchises.user_name','coupons.franchise_user_name')
                ->where('franchises.user_name',$franchise_user_name)
                ->orderBy('coupons.created_at', 'desc')
                ->paginate(10);

            // foreach ($coupon as $coupons) {
            //     if ($coupons->course_code == $course_code AND $coupons->franchise_user_name == $franchise_user_name AND $coupons->user_name == $franchise_user_name) {
            //         $users[] = $coupons;
            //     }
            // }
// print_r($users);
// die;
        $course =  Course::orderBy('created_at', 'desc')->paginate(10);
        $fran = Franchise::orderBy('created_at', 'desc')->paginate(10);



        if (isset($users)) {
            return view('thinkdashboard.admin.pages.coupon.coupon-list', compact('users','course','fran'));
        } else {
        \Session::flash('message', "No coupon Available");
        \Session::flash('alert-class', 'alert-danger');
        return back();
        }

        }

        public static function couponListingByFranchise($coursecode)
        {
            $franchise_user_name = \Session::get('franchisesession');

            $coupon_data = Coupon::where('franchise_user_name',$franchise_user_name)
                                    ->get();

            return $coupon_data;
        }
}
