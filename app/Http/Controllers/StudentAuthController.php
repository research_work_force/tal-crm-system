<?php

namespace App\Http\Controllers;

use App\UserLogin;
use App\Student;
use Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use App\Franchise;

class StudentAuthController extends Controller
{
    //create student start
    public function StudentView(){
              /*echo $course_code; die;*/
        return view('thinkdashboard.franchise.pages.student-data.studentsignup');
        }

        public function error()
        {
            $error_massege=[
            'u_name.required' => 'User Name Is Required',
            'u_name.unique:students' => 'User Name Shoud Be Unique',
            'u_name.unique:user_logins' => 'User Name Shoud Be Unique',
            'u_name.max:255' => 'User Name Should Be 255 Character Maximum',

            'email.required' => 'Email Is Required',
            'email.email' => 'Email Should be in Email Format',
            'email.max:255' => 'Email Should Be 255 Character Maximum',

            'name.required' => 'Name Is Required',
            'name.max:255' => 'Name Should Be 255 Character Maximum',

            'password.required' => 'Password Is Required',
            'password.min:6' => 'Password Should Be 6 Character Minimum',

            'contact.required' => 'Phone Number Is Required',
            'contact.digits:10' => 'Phone Number Should Be 10 Digits',

            'location.required' => 'Location Is Required',
            'location.max:255' => 'Location Should Be 255 Character Maximum',

            'g_name.required' => 'Guardian Name Is Required',
            'g_name.max:255' => 'Guardian Name Should Be 255 Character Maximum',

            'g_contact.required' => 'Guardian Contact Number Is Required',
            'g_contact.max:255' => 'Guardian Contact Number Should Be 10 Digits',

            'g_name.required' => 'Guardian Name Is Required',
            'g_name.max:255' => 'Guardian Name Should Be 255 Character Maximum',

            'g_email.required' => 'Guardian\'s Email Is Required',
            'g_email.email' => 'Guardian\'s Email Should be in Email Format',
            'g_email.max:255' => 'Guardian\'s Email Should Be 255 Character Maximum',

            'clg_name.required' => 'College Name Is Required',
            'clg_name.max:255' => 'College Name Should Be 255 Character Maximum',

            'future_goal.required' => 'Future Goal Is Required',

            'course_name.required' => 'Course Name Is Required',

            'course_duration.required' => 'Course Duration Is Required',

            'pan.required' => 'PAN Card Is Required',
            'pan.mimes:pdf,jpg,png,jpeg' => 'PAN Card Should be in PDF or Image Format',

            'reg_card.required' => 'Registration Card Is Required',
            'reg_card.mimes:pdf,jpg,png,jpeg' => 'Registration Card Should be in PDF or Image Format',

            'photo.required' => 'Photo Is Required',
            'photo.mimes:jpg,png,jpeg' => 'Photo Card Should be in Image Format',
            ];
            return $error_massege;
        }
        
       public static function studentListsForFranchise()
        {
             $data = Student::where('added_by',\Session::get('franchisesession'))->get();
   
             return $data;
        }
        /*public function showlist(Request $request)
             {   
        $d1 = $request->input('startdate');
        $d2 = $request->input('enddate');
            if(isset($d1)==1 && isset($d2)==1 )
                {

                $list = Expenses::whereBetween('created_at',[$d1,$d2])->get();
                return view('expenselist',compact('list'));
                
                }
            else{

                $list = Expenses::get();
                return view('expenselist',compact('list'));
                
                }
                 }*/ 

        //admin student list view without filter 
        public function studentViewList(){
            $student_data = Student::orderBy('created_at', 'desc')->paginate(10);              
              return view('thinkdashboard.admin.pages.students.student-list')
                                    ->with('student_data',$student_data);

        }  
   

        //admin student list view with filter(search by name)
        public  function studentListByName(request $request)
        {   
            $stu=$request->input('student-name');
                        
           if(isset($stu)==1){
            $student_data = Student::where('student_name','LIKE', '%' . $stu . '%')->orderBy("student_name")->paginate(10);
                return view('thinkdashboard.admin.pages.students.student-list',compact('student_data'));
           }
           else{
              $student_data = Student::orderBy('created_at', 'desc')->paginate(10);              
              return view('thinkdashboard.admin.pages.students.student-list')
                                    ->with('student_data',$student_data);
                }
        }

        //admin student list sorting
        public function studentListSort(request $request)
        {   
            $sort=$request->input('sortByName');
           if(isset($sort)==1){
               if($sort=="a"){
                $student_data = Student::orderBy('student_name')->paginate(10);              
                  return view('thinkdashboard.admin.pages.students.student-list')
                                        ->with('student_data',$student_data);

               }
               if($sort=="z"){
                $student_data = Student::orderBy('student_name', 'desc')->paginate(10);              
                  return view('thinkdashboard.admin.pages.students.student-list')
                                        ->with('student_data',$student_data);

               }
            }
            else{
              $student_data = Student::orderBy('created_at', 'desc')->paginate(10);              
              return view('thinkdashboard.admin.pages.students.student-list')
                                    ->with('student_data',$student_data);
                }

        }

      //admin student list view with filter(search by date)

        public function studentListByDate(request $request)
        {
            $date1=$request->input('d1');
            $date2=$request->input('d2');
            if(isset($date1)==1 && isset($date2)==1){
                $student_data = Student::whereBetween('entry_date',[$date1,$date2])->paginate(10);
                return view('thinkdashboard.admin.pages.students.student-list')
                                        ->with('student_data',$student_data);

            }
            else{
              $student_data = Student::orderBy('created_at', 'desc')->paginate(10);              
              return view('thinkdashboard.admin.pages.students.student-list')
                                    ->with('student_data',$student_data);
                }            
           
        }



         //franchise student list view 
        public static function studentLists()
        {
            $data=Student::get();
            return $data;
        }  


         public function StudentSubmit(Request $request){
            // validation checker
            //     $rules = [
            //         'uname' => 'required|unique:students|max:255',
            //         'email' => 'required|email|max:255',
            //         'name' => 'required|max:255',
            //         'password' => 'required|min:6',
            //         'contact' => 'required|digits:10',
            //         'location' => 'required|max:255',
            //         'g_name' => 'required|max:255',
            //         'g_contact' => 'required|digits:10',
            //         'g_email' => 'required|email|max:255',
            //         'clg_name' => 'required',
            //         'future_goal' => 'required',
            //         'course_name' => 'required',
            //         'course_duration' => 'required|max:255',
            //         'pan' => 'required|mimes:pdf,jpg,png,jpeg',
            //         'reg_card' => 'required|mimes:pdf,jpg,png,jpeg',
            //         'photo' => 'required|mimes:jpg,png,jpeg',
            //     ];
            // $error =$this->error();
            // $this->validate($request, $rules, $error);
             $franchise_loc = Franchise::where('user_name',\Session::get('franchisesession'))->first();

             $loc = strtoupper(substr($franchise_loc->location, 0, 3));
 
             // format = TAL/YEAR/MONTH/LOCATION/RANDOM NUMBER
             $student_code = "TAL/".Carbon::now()->year."/".Carbon::now()->month."/".$loc."/".mt_rand(1,9999);

                    //save in database of admin
             $student_user = Student::where("student_code", $student_code)->first();

             if(empty($student_user)) {

                 $student_user = new Student();
                 $student_user->student_code = $student_code;
                 $student_user->email = $request->input('email');
                 $student_user->student_name = $request->input('student_name');
                 $student_user->contact = $request->input('contact');
                 $student_user->address = $request->input('address');
                 $student_user->g_name = $request->input('g_name');
                 $student_user->g_contact = $request->input('g_contact');
                 $student_user->g_email = $request->input('g_email');
                 $student_user->institute_name = $request->input('institue_name');
                 $student_user->future_goal = $request->input('future_goal');
                 $student_user->course_code = $request->input('course_code');

                 if ($request->hasFile('adhaar_card')|$request->hasFile('photo')|$request->hasFile('other_docs')) {
        // Document Adhaar card
                    $adhaar_card = $request->file('adhaar_card');
                    $directory_adhaar_card = 'upload/student/adhaar_card'.'/'.$request->input('student_code').'/';
                    $filename_adhaar_card = time() . '.' . $adhaar_card->getClientOriginalExtension();

           
                    $path_adhaar_card = $adhaar_card->move($directory_adhaar_card, $filename_adhaar_card);

        // Document Photo

                   $photo = $request->file('photo');
                   $directory_photo = 'upload/student/photo'.'/'.$request->input('student_code').'/';
                   $filename_photo = time() . '.' . $photo->getClientOriginalExtension();

                

                   $path_photo = $photo->move($directory_photo, $filename_photo);

        // Document other docs
                 if($request->hasFile('other_docs') != null){
                     $other_docs = $request->file('other_docs');
                     $directory_other_docs = 'upload/student/other_docs'.'/'.$request->input('student_code').'/';
                     $filename_other_docs = time() . '.' . $other_docs->getClientOriginalExtension();

          
                     $path_other_docs = $other_docs->move($directory_other_docs, $filename_other_docs);
                      }
                     else{
                          $filename_other_docs = "default.png";
                         }

                  }


                $student_user->other_docs = $filename_other_docs;
                $student_user->adhaar_card = $filename_adhaar_card;
                $student_user->photo = $filename_photo;
                $student_user->added_by = \Session::get('franchisesession');
                $student_user->save();



                 \Session::flash('message', "Course has been added to Queue. Complete your payment Process");
                 \Session::flash('alert-class', 'alert-success');

                  return $student_code;
                }

                 else{

                    \Session::flash('message', "Something went wrong. Try again later.");
                    return 0;
                   }

            }
    //create student end


            //student login
        public function LoginView(){
            return view('thinkdashboard.franchise.student.pages.login');
            }

        public function LoginSubmit(Request $request){

                // validation checker
            $validatedData = $request->validate([
                'u_name' => 'required|max:255',
                'password' => 'required|min:6',
            ]);

            // save in variable
            $u_name = $request->input('u_name');
            $password = $request->input('password');
            $hashpassword = Hash::make($password);

            //check in database of login
            $login_users = UserLogin::where("u_name",$u_name)->first();

            if(empty($login_users)) {

                \Session::flash('message', "You enter wrong User Name");
                \Session::flash('alert-class', 'alert-danger');

                return redirect('/student/login');
            }
            else{

                if(Hash::check($password,$login_users['password'])) {

                    \Session::flash('message', "You are Loged In");
                    \Session::flash('alert-class', 'alert-success');
                    session(['studentsession' => $u_name]);
                    return redirect('/student/dashboard');
                }
                else{
                    \Session::flash('message', "You enter wrong Password");
                    \Session::flash('alert-class', 'alert-danger');

                    return redirect('/student/login');

                    }

                }
            }
            //student list
            public function studentListByFranchise()
            {
                        $student_data = Student::where('added_by',\Session::get('franchisesession'))
                                                ->orderBy('created_at', 'desc')
                                                ->paginate(10);

                        return view('thinkdashboard.franchise.pages.students.student-list')
                                    ->with('student_data',$student_data);
            }



            //student list
            public static function studentListByStudentCode($stdt_code)
            {
                        $student_data = Student::where('student_code',$stdt_code)
                                                ->first();

                        return $student_data;
            }



            //block unblock
            public function block($u_name)
            {
                        $student_status = UserLogin::where('u_name',$u_name)->first();

                        if ($student_status->status == 1) {
                            $student_status->status = "0";
                        }
                        else {
                            $student_status->status = "1";
                        }
                        $student_status->save();

                    return back();
            }

                //student update start
        public function updateStudentInformation(Request $request)
                {
                    // $rules = [
                    //     'name' => 'required|max:255',
                    //     'contact' => 'required|digits:10',
                    //     'location' => 'required|max:255',
                    //     'g_name' => 'required|max:255',
                    //     'g_contact' => 'required|digits:10|integer',
                    //     'g_email' => 'required|email|max:255',
                    //     'clg_name' => 'required|max:255',
                    //     'future_goal' => 'required|max:255',
                    //     'course_name' => 'required|max:255',
                    //     'course_duration' => 'required|max:255',
                    //     'pan' => 'nullable|mimes:pdf,jpg,png,jpeg',
                    //     'reg_card' => 'nullable|mimes:pdf,jpg,png,jpeg',
                    //     'photo' => 'nullable|mimes:jpg,png,jpeg',
                    // ];
                    // $error_massege =$this->error();
                    // $this->validate($request, $rules, $error_massege);

                    $stdt_code = $request->input('stdt_code');

             $student_data = Student::where('student_code', $stdt_code)->first();
     
             if(!empty($student_data)) {

             $student_data->email = $request->input('email');
             $student_data->student_name = $request->input('student_name');
             $student_data->contact = $request->input('contact');
             $student_data->address = $request->input('address');
             $student_data->g_name = $request->input('g_name');
             $student_data->g_contact = $request->input('g_contact');
             $student_data->g_email = $request->input('g_email');
             $student_data->institute_name = $request->input('institute_name');
             $student_data->future_goal = $request->input('future_goal');

            //  if ($request->hasFile('pan')|$request->hasFile('reg_card')|$request->hasFile('photo')) {

            //     $data_pan = $student->pan;
            //     list($pub,$upload,$student,$docu,$year,$month,$name)=explode("/",$data_pan);
            //     $docs_pan = "storage/".$upload."/".$student."/".$docu."/".$year."/".$month."/".$name;
            //     File::delete($docs_pan);

            //     $pan = $request->file('pan');
            //     $directory_pan = 'public/upload/student/pan'.'/'.date("Y").'/'.date("M").'/';
            //     $filename_pan = $directory_pan.time() . '.' . $pan->getClientOriginalExtension();

            //     if(!is_dir($directory_pan)){
            //             Storage::makeDirectory($directory_pan);
            //     }

            //     $path_pan = $request->pan->storeAs('', $filename_pan);
            //     $student->pan = $filename_pan;

            //     $data_reg = $student->reg_card;
            //     list($pub,$upload,$student,$docu,$year,$month,$name)=explode("/",$data_reg);
            //     $docs_reg = "storage/".$upload."/".$student."/".$docu."/".$year."/".$month."/".$name;
            //     File::delete($docs_reg);

            //     $reg_card = $request->file('reg_card');
            //     $directory_reg = 'public/upload/student/reg_card'.'/'.date("Y").'/'.date("M").'/';
            //     $filename_reg = $directory_reg.time() . '.' . $reg_card->getClientOriginalExtension();

            //     if(!is_dir($directory_reg)){
            //         Storage::makeDirectory($directory_reg);
            //     }

            //     $path_reg = $request->reg_card->storeAs('', $filename_reg);
            //     $student->reg_card = $filename_reg;

            //     $data_photo = $student->photo;
            //     list($pub,$upload,$student,$docu,$year,$month,$name)=explode("/",$data_photo);
            //     $docs_photo = "storage/".$upload."/".$student."/".$docu."/".$year."/".$month."/".$name;
            //     File::delete($docs_photo);

            //     $photo = $request->file('photo');
            //     $directory_photo = 'public/upload/student/photo'.'/'.date("Y").'/'.date("M").'/';
            //     $filename_photo = $directory_photo.time() . '.' . $photo->getClientOriginalExtension();

            //     if(!is_dir($directory_photo)){
            //         Storage::makeDirectory($directory_photo);
            //     }

            //     $path_photo = $request->photo->storeAs('', $filename_photo);
            //     $student->photo = $filename_photo;
            // }

            $student_data->save();

                \Session::flash('message', "Student Details has been updated");
                \Session::flash('alert-class', 'alert-info');
                return redirect()->back();

                }
            }
                //student update end

                //student delete start
                public function StudentDelete($id)
                {
                $student = Student::find($id);
                $student_login = StudentLogin::find($id);

                $data_pan = $student->pan;
                list($pub,$upload,$student,$docu,$year,$month,$name)=explode("/",$data_pan);
                $docs_pan = "storage/".$upload."/".$student."/".$docu."/".$year."/".$month."/".$name;
                File::delete($docs_pan);

                $data_reg = $student->reg_card;
                list($pub,$upload,$student,$docu,$year,$month,$name)=explode("/",$data_reg);
                $docs_reg = "storage/".$upload."/".$student."/".$docu."/".$year."/".$month."/".$name;
                File::delete($docs_reg);

                $data_photo = $student->photo;
                list($pub,$upload,$student,$docu,$year,$month,$name)=explode("/",$data_photo);
                $docs_photo = "storage/".$upload."/".$student."/".$docu."/".$year."/".$month."/".$name;
                File::delete($docs_photo);

                $student->delete();
                $student_login->delete();

                \Session::flash('message', 'student details has been deleted');
                \Session::flash('alert-class', 'alert-info');
                return redirect()->back();
            }
                //student delete end

                //student logout start
                public function Logout(){
                    \Session::flush();
                    \Session::flash('message', "You are Loged Out");
                    \Session::flash('alert-class', 'alert-danger');

                    return redirect('/student/login');
                    }
                //student logout end


}
