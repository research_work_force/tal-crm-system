<?php

namespace App\Http\Middleware;

use Closure;

class FranchiseSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('franchisesession')) {

            return $next($request);
        }
        else{
            return redirect('/franchise/login');
        }
    }
}
